---
title: "Marcon, not Le Pen"
date: "2017-05-08T10:59:00+10:00"
year: "2017"
location: Sydney
abstract: "Brexit, Trump, hard-right populism? Non!"
year: "2017"
location: Sydney
category: Thoughts
tag:
- france
- news
- politics
---
France looked at Brexit, Trump, and hard-right populism, and said *non!* I've been threatening to stop political commentary on *Rubénerd*, but what great news to start the week.


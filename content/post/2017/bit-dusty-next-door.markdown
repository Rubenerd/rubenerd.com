---
title: "Video: Bit dusty next door"
date: "2017-09-18T14:14:28+10:00"
year: "2017"
location: Sydney
abstract: "So that’s why our office balcony was covered in grit this morning!"
year: "2017"
location: Sydney
category: Media
tag:
- australia
- sydney
---
<p></p>

<iframe src="https://player.vimeo.com/video/234257069" style="width:500px; height:281px; border:0;"></iframe>

So that's why our office balcony was [covered in grit] this morning!

[covered in grit]: https://vimeo.com/234257069 "Video of earth mover kicking up dust, on Vimeo"


---
title: "Rubénerd Law of Headlines"
date: "2017-09-14T19:01:00+10:00"
summary: "There are legit, un-harmful ways to use tech in headlines stating they're considered harmful"
year: "2017"
location: Sydney
category: Media
tag:
- news
---
Betteridge's Law of Headlines states that the answer to headings ending in a question is no. For example, "Does X cure cancer?" wouldn't need the question mark if the answer was yes. It's an exercise in journalistic hedge betting.

In similar spirit, I propose the Rubénerd Law of Headlines. If an article is titled with *"Technology foo considered harmful"*, there's an unharmful, legitimate use for it. The author may even discuss it in their article.

These headings are marginally better than *"you won't believe why this tech is so harmful!"*, but still clickbait.


---
title: "Ogden Nash #1"
date: "2017-06-15T13:05:00+10:00"
year: "2017"
location: Sydney
abstract: "On trees"
year: "2017"
location: Sydney
category: Thoughts
tag:
- silly
- ogden-nash
- poems
---
> I think that I shall never see  
> A billboard lovely as a tree  
> Indeed, unless the billboards fall  
> I'll never see a tree at all.


---
title: "Jerry Novak's June coffee"
date: "2017-06-07T09:05:00+10:00"
year: "2017"
location: Sydney
abstract: "Thank you for the latest coffee!"
year: "2017"
location: Sydney
category: Thoughts
tag:
- coffee
- jerry-novak
- weblog
---
I wanted to give a shoutout and personal thanks to the Official Rubenerd Patron Sir Jerry Novak again for another caffeine-related donation. I'm drinking it as we speak!

I may be working from San Francisco for a couple of months later this year, I'm thinking a quick trip down to LA for a meetup may be in order :).


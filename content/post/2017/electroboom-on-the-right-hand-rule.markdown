---
title: "Electroboom on the Right Hand Rule"
date: "2017-09-08T14:27:30+10:00"
year: "2017"
location: Sydney
abstract: "One of the greatest videos by one of the greatest YouTubers"
year: "2017"
location: Sydney
category: Hardware
tag:
tag:
- electronics
- mehdi-sadaghdar
- electroboom
- youtube
---
*[ElectroBOOM]* is the world's greatest YouTube Channel, [right behind mine] where I replaced the fan on my first generation MacBook Pro nine years ago. Don't watch that.

Mehdi Sadaghdar is hilarious, educational, and [FULL BRIDGE RECTIFIER]. I'm a proud [Patreon subscriber], and you should be too.

<iframe style="width:500px; height:281px; border:0;" src="https://www.youtube.com/embed/NJRDclzi5Vg?start=69"></iframe>

For you Friday viewing pleasure, [have this earworm] that's been stuck in my head all day. Mission accomplished!

[ElectroBOOM]: https://www.youtube.com/channel/UCJ0-OtVpF0wOKEqT2Z1HEtA
[FULL BRIDGE RECTIFIER]: https://www.youtube.com/watch?v=sI5Ftm1-jik
[Patreon subscriber]: http://www.patreon.com/electroboom
[have this earworm]: https://www.youtube.com/watch?v=NJRDclzi5Vg
[right behind mine]: https://www.youtube.com/user/rubenerd


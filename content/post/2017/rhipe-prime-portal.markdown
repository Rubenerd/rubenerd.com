---
title: "Rhipe Prime Portal"
date: "2017-02-14T10:04:00+10:00"
year: "2017"
location: Sydney
abstract: "Somehow, their stock photo guy looks less than impressed"
year: "2017"
location: Sydney
category: Internet
tag:
- australia
- channel
- cloud-computing
- licencing
- microsoft
---
<p><a href="https://www.primeportal.online/"><img src="https://rubenerd.com/files/2017/RhipeFullImage@1x.jpg" alt="Stock photo of a man standing in front of a blackboard looking concerned." style="width:500px; height:300px" srcset="https://rubenerd.com/files/2017/RhipeFullImage@1x.jpg 1x, https://rubenerd.com/files/2017/RhipeFullImage@2x.jpg 2x" /></a></p>

Somehow, their stock-photo guy looks less than impressed.


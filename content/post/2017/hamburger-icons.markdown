---
title: "Hamburger icons"
date: "2017-07-04T08:23:00+10:00"
year: "2017"
location: Sydney
abstract: "Hamburger/meatstack icons are top of the list"
year: "2017"
location: Sydney
category: Internet
tag:
---
There is only one permissible use, and that's to render commander–rank, quantum–universe Star Trek combadges:

> ☰/\☰

Otherwise, for all that is good and wholesome: **stop using them**.


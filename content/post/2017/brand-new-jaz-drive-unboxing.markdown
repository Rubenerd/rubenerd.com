---
title: "Unboxing a 1997 Iomega Jaz Drive"
date: "2017-05-12T15:39:00+10:00"
year: "2017"
location: Sydney
abstract: "My first unboxing, naturally for a twenty-year-old disk drive!"
year: "2017"
location: Sydney
category: Hardware
tag:
- iomega
- jaz
- unboxing
- video
---
<p></p>

<iframe src="https://player.vimeo.com/video/217124964" style="width:500px; height:271px; border:0"></iframe>

I did my [first unboxing video] last month, and naturally it was for a twenty year old disk drive!

I'd always wanted an Iomega Jaz Drive growing up, but I could never justify the size or cost. Now they're old enough to be cheap, so I picked one up sealed on eBay for $30.

[first unboxing video]: https://vimeo.com/217124964


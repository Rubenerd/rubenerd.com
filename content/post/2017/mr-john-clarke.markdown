---
title: "Mr John Clarke, one of the great satirists"
date: "2017-05-02T08:25:00+10:00"
year: "2017"
location: Sydney
abstract: "Thanks for your time"
year: "2017"
location: Sydney
category: Media
tag:
- australia
- clark-and-dawe
- new-zealand
---
The witty, fabulous interviewee from the legendary Clarke and Dawe duo passed on last month, and I've been dragging my heels posting because nothing I write seems to do the bloke justice.

His sharp, dry wit aside, I loved his uncanny ability to reproduce the talking points and attitudes of his subjects, without needing to mimic their voices. He just weaponised their words.

Clarke and Dawe's three minute skits have been a fixture of Australian TV for decades, and most of their material is now [on their YouTube page]. I'd recommend every single one, but here are just a few of my recent favourites, along with arguably the all time classic at the end.

Thanks for your time, Mr John Clarke.

<iframe style="width:500px; height:281px; border:0;" src="https://www.youtube.com/embed/lQoT9xXRXtY"></iframe>

<iframe style="width:500px; height:281px; border:0;" src="https://www.youtube.com/embed/w-MMUMPJUQI"></iframe>

<iframe style="width:500px; height:281px; border:0;" src="https://www.youtube.com/embed/ELaBzj7cn14"></iframe>

<iframe style="width:500px; height:281px; border:0;" src="https://www.youtube.com/embed/I5QwKEwo4Bc"></iframe>

<iframe style="width:500px; height:281px; border:0;" src="https://www.youtube.com/embed/3m5qxZm_JqM"></iframe>

[on their YouTube page]: https://www.youtube.com/channel/UCPyb1dDiGoZ07j_DKzam4sQ


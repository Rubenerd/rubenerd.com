---
draft: true
title: "Looking outside the Apple camp"
date: "2018-08-27T08:45:43+10:00"
abstract: "The tick-tock cycle of Apple hardware continues."
year: "2018"
location: Sydney
category: Hardware
tag:
- apple
- design
- macbook
---
I got my first Apple computer in 1999: a shiny blueberry iMac DV. The one with the handle, and honking CRT. I saw it being demoed at COMDEX in Singapore, and was reassured by Virtual PC running Windows 98 on it. I

 That Christmas I fired it up, and realised Mac OS 8.6 was **so much better** it was laughable. I kept a Windows 2000 desktop around, but by that point the fruit was bitten.

Since then PCs have been relegated to servers, or in the case of my Pentium MMX tower, nostalgia. Relegated may be too negative a term; thesedays FreeBSD and ZFS is the only operating and file systems I trust to protect my data, and by extension, memories. NetBSD makes a sweet network appliance. And Linux, when its not cutting off its nose to spite its face, is easier to install and maintain in 2018 than Windows.

But being part of this curated Apple ecosystem for two decades has led me to observe an Intel-esque tick-tock to their design. They go through periods where they absolutely *nail* it, then they drop the hammer on their feet a few times just for the fun of it, before bandaging up their toes and setting their sights on the wall again. 

[To quote Doc Searls], whom I still read after all these years:

> I’m writing this on a new Apple laptop that cost more than my last car and it is thick with bad ideas and gone features that no customer would ever have demanded: 1) clackety keyboard in place of the old near-silent one, 2) oversized and oversensitive trackpad, 3) the brilliant and handy MagSafe power connector replaced by a USB-C one and a cord that not only fails to protect itself and the laptop when one trips over it, but has no charge indicator light, 4) gone USB-A, Thunderbolt and SD card port, 5) useful function keys replaced by the mostly-useless and fully annoying TouchBar and 6) shipped with no dongles to make up for lost ports, and not even a power cord from the adapter to the wall.

They'll come back around, they always do. But gosh it feels like its taking them longer this time.

[To quote Doc Searls]: https://blogs.harvard.edu/doc/2018/08/08/presuming-competence/#comment-350717


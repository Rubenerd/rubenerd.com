---
draft: true
title: "Gigabyte BIOS beeps"
date: "2018-08-20T13:18:58+10:00"
abstract: "The official Gigabyte FAQ page is inscrutable, so I reformatted it."
year: "2018"
location: Sydney
category: Hardware
tag:
- 
- motherboards
---
I was finding the [official Gigabyte FAQ] page *inscrutable*, so I reformatted it here for your reference and mine.

### Award BIOS

* 1 short beep: system normal 
* 2 short beep: CMOS error
* 1 long beep and 1 short beep: memory error
* 1 long beep and 2 short beeps: graphic card error
* 1 long beep and 3 short beeps: AGP error
* 1 long beep and 9 short beeps: memory error
* Continuous long beep: memory not correctly installed
* Continuous short beep: power supply unit failed

### AMI BIOS

* 1 short beep: memory error
* 2 short beep: memory parity check error.
* 3 short beep: basic memory 64K address check error
* 4 short beep: real time clock malfunction
* 5 short beep: CPU error
* 6 short beep: keyboard error
* 7 short beep: CPU interruption error 
* 8 short beep: hraphic card error
* 9 short beep: memory error
* 10 short beep: CMOS error
* 11 short beep: CPU cache memory malfunction

[official Gigabyte FAQ]: https://www.gigabyte.com/Support/FAQ/816


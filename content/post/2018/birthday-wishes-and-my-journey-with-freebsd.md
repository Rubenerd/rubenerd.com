---
draft: true
title: "Birthday wishes, and my journey with FreeBSD"
date: "2018-06-20T23:41:56-07:00"
abstract: ""
thumb: "https://rubenerd.com/files/2018/"
year: "2018"
location: Sydney
category: Thoughts
tag:
- 
---
Few things have had as much of a positive impact on my personal and professional lives as the wonderful FreeBSD operating system and its community. So I thought it fitting to dedicate a post to it on its 25th anniversay.

### My first exposure

Any technical blog post here necessarily entails a bit of pointless nostalgia and reflection on my part, and this will be no different!

I grew up on DOS/Windows 3.x, then Windows 95. I used to joke that people born in the mid to late 1980s arrived too late for the fascinating home computer revolution, but too early for the world wide web. Later I got a copy of Red Hat Linux on CD-ROM, and was installing it on machines I built myself.

Within a few years I'd replaced both: Windows with the classic Macintosh, and Linux with FreeBSD. They couldn't have been any more different, but I moved to them for frustratingly intangible reasons.

In fact much of my blog from 2006 was my exploration of FreeBSD on the desktop

Where Linux felt modular and, FreeBSD feels *engineered*. It's a frutratingly intangible quality to describe, but BSD readers can likely emphatise. Everything from the single source tree, excellent documentation, 

The Handbook stands as one of the best online techniucal reference works I've ever had the pleasure.

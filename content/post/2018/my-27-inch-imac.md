---
draft: true
title: "My 27-inch iMac, six months in"
date: "2018-08-19T10:21:06+10:00"
abstract: ""
thumb: ""
year: "2018"
location: Sydney
category: Hardware
tag:
- 
---
I bought my 27" iMac back in February. The new-car priced iMac Pro aside, this model is the only Mac in 2018 with decent graphics. The Radeon 580 doesn't win benchmarks, but it renders 1440p Train Simulator and 4k Cities Skylines beautifully.

It also spared me from this:

> More importantly, it meant I could get rid of no fewer than six cables, two power bricks, an extra cable organiser box, and got almost a third of my desk space back. I could also finally swap out my huge powerboard for a smaller one that fits in the remaining cable box much more easily, and may be less of an imminent fire risk! Also, no more flaky Thunderbolt dock!

I've also almost never used her. I work late into the night most days, and on the weekend I prefer exploring or hanging out in coffee shops writing code or blogging. I play the above games maybe an hour or two a month, if that.

Then working in San Francisco, I didn't miss it at all.

Do I sell it, and trade for a laptop? The only issue there is the new MacBook Pros still have lower end graphics than this machine, for several thousand dollars more. A T-series ThinkPad with FreeBSD and bhyve would be cheaper for equivalent specs; but that's still another upfront investment.

Damn it, my former colleague might have been right.


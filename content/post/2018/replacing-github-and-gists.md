---
draft: true
title: "Replacing GitHub and Gists"
date: "2018-06-06T15:45:24+10:00"
abstract: "Bitbucket, GitLab, or self host?"
thumb: "https://rubenerd.com/files/2018/gh-to-bitbucket@1x.png"
year: "2018"
location: Sydney
category: Software
tag:
- git
- mercurial
---
<p><img src="https://rubenerd.com/files/2018/gh-to-bitbucket@1x.png" srcset="https://rubenerd.com/files/2018/gh-to-bitbucket@1x.png 1x, https://rubenerd.com/files/2018/gh-to-bitbucket@2x.png 2x" alt="GitHub to Bitbucket graphic from Atlassian" style="width:494px; height:169px;" /></p>

Microsoft bought GitHub. Let's check out replacements:

**[Bitbucket]**
: I used to host on there, in part because [hg is way nicer than Git]. I [moved to GitHub] to learn Git, and because I wanted to contribute to projects there.<p></p>

**[GitLab]**
: This is the new tech darling, but it feels like I'd be kicking the proverbial can down the road. Who's to say that won't also be bought and Skypeified?<p></p>

**[GitWeb]**
: This built-in CGI web interface is bare bones, and I'd miss things like <abbr title="permanent resident, wait, pull request">PRs</abbr>, but maybe its worth going back to basics?<p></p>

**[Self-host]**
: Should be easy enough to do. [Gitorius] looked the most promising last time I looked, but it's been rolled into [self-hosted GitLab]! Maybe I should just use that. You there, reading this! Any suggestions?

It all comes back to when I [moved off GitHub Pages in 2013], albeit with slightly different motives this time:

> It was a fun experiment, but as I’ve repeatedly learned over the years when using other services, I always come back to self hosting. For my needs, it’s just a better fit.

What's unclear is what to replace [GitHub Gists] with. I've been using that as an outboard brain for text files and scripts for years, and find it incredibly useful. Maybe I'll just go back to a *Useful Scripts Desu~* repo for all that again.

[hg is way nicer than Git]: http://stevelosh.com/blog/2010/01/the-real-difference-between-mercurial-and-git/ "Steve Losh: The Real Difference Between Mercurial and Git"
[Bitbucket]: https://bitbucket.org/
[GitLab]: https://about.gitlab.com/
[Gitorius]: https://en.wikipedia.org/wiki/Gitorious
[self-hosted GitLab]: https://www.freshports.org/www/gitlab
[GitWeb]: https://git-scm.com/book/en/v2/Git-on-the-Server-GitWeb
[Self-host]: https://rubenerd.com/goodbye-digg-reader/ "Goodbye Digg Reader: the last time I thought of self-hosting"
[GitHub Gists]: https://gist.github.com/rubenerd
[moved off GitHub Pages in 2013]: http://localhost:1313/goodbye-github-pages/
[moved to GitHub]: https://rubenerd.com/importing-from-bitbucket-to-github/ "Rubenerd: Importing from Bitbucket to Github"


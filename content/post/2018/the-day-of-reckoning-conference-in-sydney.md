---
draft: true
title: "The Day of Reckoning conference in Sydney"
date: "2018-08-18T10:37:06+10:00"
abstract: ""
thumb: ""
year: "2018"
location: Sydney
category: Thoughts
tag:
- 
---
Friends and I last weekend went to see The Day of Reckoning, a conference around free thought and ideas. The speakers included Sam Harris, Maajid Nawaaz

I'll admit, I reached immediately for the ticket purchase button for the first two speakers, but each had fascinating, rational, and what I'd consider perfectly reasonable viewpoints on a range of topics that left me afterwards with more questions. Mission success!

I've been an avid Sam Harris fan since his seminal *The End of Faith* and *Letter to a Christian Nation* that I read during my high school exploration into atheism. These came on the wave of the Four Horseman including Hitchens, Dawkins, and Dennet, but his neurological background and unique writing style afforded him a unique take I found fascinating.

Sam has since moved on to topics he almost certainly wanted to discuss from the beginning, from free will and lying, to how to be ethical and engage in spirituality without religion. His podcast Waking Up is an absolute must listen, with a range of guests with whom I often don't agree, but are always thought provoking.

I subsequently learned of Maajid Nawaaz though Sam's podcast and their collaborative book *Islam: The Future of Tolerance*. Maajid runs Quillium in the UK to counter Islamist ideas and provide councelling. He would know how it all works; he most famously spent time in an Egyptian prison as a member of HT. He has a sincere  

XXXX coined the term Intellectual Dark Web as a tongue in cheek reference to

If I've learned anything from this conference, and either reading or listening to these people, it's that it's critical to hear perspectives from outside your bubble.
 

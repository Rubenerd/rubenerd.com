---
title: "A decade of Yuletide Steins;Gate"
date: "2018-12-01T08:34:18+11:00"
abstract: "Another reason I feel old!"
year: "2018"
location: Sydney
category: Anime
tag:
- yuletide
- steins-gate
- desktop-backgrounds
---
It's another Yuletide season! And it reminds me of [this desktop background] by Karafuru Sekai Designs of everyone's favourite moe microwave time travelling scientists I first started using in 2013. That was a long sentence, unlike this one.

<p><img src="https://rubenerd.com/files/2018/welcome_to_2012-wdr@1x.jpg" srcset="https://rubenerd.com/files/2018/welcome_to_2012-wdr@1x.jpg 1x, https://rubenerd.com/files/2018/welcome_to_2012-wdr@2x.jpg 2x" alt="" style="width:500px" /></p>

It's now more than a decade since the first date shown. I feel old, though somehow no less fabulous.

[this desktop background]: https://rubenerd.com/yuletide-steins-gate/

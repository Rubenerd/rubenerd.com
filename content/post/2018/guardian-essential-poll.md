---
draft: true
title: "Guardian Essential Poll"
date: "2018-08-30T08:46:20+10:00"
abstract: ""
thumb: ""
year: "2018"
location: Sydney
category: Thoughts
tag:
- 
---

> Here is a list of things both favourable and unfavourable that have been said about various political parties. Which statements do you feel fit the Liberal Party?

Divided.
: Yes.

Out of touch with ordinary people.
: Yes.

Will promise to do anything to win votes.
: Yes, but not unique to them.

Too close to the big corporate and financial interests.
: Yes.

Moderate.
: No.

Have a vision for the future.
: Yes, its just not a good one.

Have good policies.
: For who?

Understands the problems facing Australia.
: No.

Extreme.
: Yes.

Clear about what they stand for.
: Yes, you should know what you're going to get.

Looks after the interests of working people.
: Haha!

Has a good team of leaders.
: No.

Trustworthy. 
: Yes. You get exactly what you should expect.

Keeps its promises.
: To who?

79%	23	56%
69%	5	64%
	68%	4	64%
67%	-	67%
48%	-8	56%
	43%	-9	52%
40%	-6	46%
40%	-2	42%
40%	6	34%
	33%	-12	45%
32%	-5	37%
31%	-14	45%
	30%	-4	34%
28%	-5	33%



---------------

osi controller went offline

272


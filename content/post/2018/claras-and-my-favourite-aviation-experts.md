---
draft: true
title: "Clara’s and my favourite aviation experts"
date: "2018-08-20T12:38:50+10:00"
abstract: "The people who come up on Mayday and other shows"
year: "2018"
location: Sydney
category: Hardware
tag:
- aviation
- travel
---
Clara and I watch a lot of *Mayday* and other air crash investigation shows. It's reassuring that even in the midst of tragedy, lessons are learned and aviation is made safer.

Our favourite people that keep coming up include John M. Cox at [Safety Operating Systems]\:

<p><img src="https://rubenerd.com/files/2018/aviation-cox.jpg" alt="John Cox" style="width:75px; height:98px; float:right; margin:0 0 1em 2em" /></p>

> A veteran major airline, corporate and general aviation pilot, Captain John Cox has flown over 14,000 hours with over 10,000 in command of jet airliners. Additionally, he has flown as an instructor, check pilot, and test pilot in addition to his extensive involvement in global air safety.

Gregory A. Feith at [Air Crash Detective]\:

<p><img src="https://rubenerd.com/files/2018/aviation-feith.jpg" alt="Gregory A. Feith" style="width:75px; height:98px; float:right; margin:0 0 1em 2em" /></p>

> Gregory A. Feith is a former Senior Air Safety Investigator with the National Transportation Safety Board. During his time at the NTSB, Greg Feith worked as an Air Safety Investigator Field Unit Supervisor, Regional Director and Senior Air Safety Investigator.

And [John J. Nance], Clara's and my favourite aviation expert, is a prolific author of non-fiction and investigative fiction. I had no idea!

<p><img src="https://rubenerd.com/files/2018/aviation-nance.jpg" alt="John J. Nance" style="width:75px; height:98px; float:right; margin:0 0 1em 2em" /></p>

> For over 30 years, John has served as a leader and a pioneer in both aviation and medical safety and quality, and for the past 18 years he has become a familiar face to North American television audiences as the Aviation Analyst for ABC World News and Good Morning America.

[Safety Operating Systems]: http://www.safeopsys.com/staff/captain-john-m-cox/
[John J. Nance]: http://www.johnnanceassociates.com/
[Air Crash Detective]: http://aircrashdetective.com/


---
title: "Norah Jones, Sunrise"
date: "2018-01-05T09:06:25+11:00"
abstract: "Still one of my favourite songs of all time, but I'd never seen the music video!"
year: "2018"
location: Sydney
category: Media
tag:
- music
- jazz
- norah-jones
- nostalgia
- postaday2018
---
<p></p>

<iframe style="width:500px; height:275px; border:0px;" src="https://www.youtube.com/embed/fd02pGJx0s0"></iframe>

This is among my favourite songs of all time, but until today I hadn't seen the [music video]. This is one of the most adorable, wonderful things ever.

I [started this blog] and graduated high school in 2004, when this song came out. Norah Jones and Michael Franks got me through those final exams and assignments. It already seems like such a different world ago.

Given the fun the global IT community is having right now, felt it was as good a time as any to share this.

[music video]: https://www.youtube.com/fd02pGJx0s0
[started this blog]: https://rubenerd.com/the-first-post/ "The first post on Rubenerd!"


---
draft: true
title: "Don’t use seasons to denote dates"
date: "2018-08-07T13:51:50+10:00"
abstract: ""
thumb: ""
year: "2018"
location: Sydney
category: Thoughts
tag:
- 
---
There aren't that many Americanisms that confound me, but their use of seasons to denote time is one of them. [CRN Australia] ran this article from [CRN America]\:

> The Nippon Telegraph & Telephone group (NTT) is creating a new company to oversee its three communications and data subsidiaries, creating one of Japan's biggest IT solution providers, according to a report.
> 
> The Nikkei Asian Review said NTT will launch the new company this spring.

Spring here, or spring in the north? Did they translate it, or was it copied verbatim? I checked, and the original article did say *in the fall*, or their autumn as we'd say. So use of spring was correct.

This demonstrates why denoting time with seasons is a bad idea. We have months and quarters already that are succient, accurate, and unambiguous.

Another example: people in the tropics. When I lived in Singapore, there were no disernable seasons at all in the temporate sense. *Winter* to *spring* is meaningless.

Friends, don't let friends use seasons to denote dates. At best, it's poor <abbr title="internationalisation">i18n</abbr>.

[CRN Australia]: https://www.crn.com.au/news/ntt-to-merge-dimension-data-into-new-company-report-499642
[CRN America]: https://www.crn.com/news/channel-programs/300107551/ntt-to-launch-giant-japanese-solution-provider-this-fall-report.htm


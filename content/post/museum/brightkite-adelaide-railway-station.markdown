---
title: "#Brightkite: Adelaide Railway Station"
date: "2009-08-31T05:25:42-06:00"
location: Adelaide
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=138.59649628400803%2C-34.92210958754432%2C138.59810829162598%2C-34.9204667489753&amp;layer=mapnik"></iframe>

Checked into Adelaide Railway Station (Adelaide Railway Station, Adelaide SA 5000, Australia).

* [Location URL](http://brightkite.com/places/9d9fb0c103e11deb4e3003048c0801e)
* [Checkin URL](http://brightkite.com/objects/48d2a20962111de93d1003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=19/-34.92129/138.59730)


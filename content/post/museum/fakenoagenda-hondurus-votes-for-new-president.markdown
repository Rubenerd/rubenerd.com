---
title: "#FakeNoAgenda: Hondurus votes for a new president"
date: "2013-11-25T02:23:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[From DW](http://www.dw.de/hondurans-vote-for-president-as-country-struggles-with-violence-and-poverty/a-17249121?maca=en-TWITTER-EN-2004-xml-mrss):

> Sunday's vote pits the leftist wife of Honduras' deposed president, Xiomara Castro, against the head of Congress, Juan Orlando Hernandez, of the ruling conservative National Party. Polls put the two candidates in a statistical tie heading into the election, though Castro is regarded as the slight favorite ahead of Hernandez.

A leftist... called Castro? Only your [Fake] No Agenda could have uncovered this obvious connection.


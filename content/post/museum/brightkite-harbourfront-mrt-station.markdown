---
title: "#Brightkite: Harbourfront MRT Station"
date: "2009-07-14T19:08:31-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.8205298781395%2C1.26489943099538%2C103.82214188575745%2C1.2669025521171047&amp;layer=mapnik"></iframe>

Checked into Harbourfront MRT Station (Bukit Merah, Singapore).

* [Location URL](http://brightkite.com/places/733fc954d88b11dd9b2e003048c10834)
* [Checkin URL](http://brightkite.com/objects/2f0ee1e70dc11de850c003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.26590/103.82134)


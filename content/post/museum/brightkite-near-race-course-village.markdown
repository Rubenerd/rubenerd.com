---
title: "#Brightkite: Near Race Course Village"
date: "2009-07-17T02:02:56-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.85217726230621%2C1.310911827473%2C103.85540127754211%2C1.3149179957249795&amp;layer=mapnik"></iframe>

Checked into near Race Course Village, , Singapore.

* [Location URL](http://brightkite.com/places/3b7ee8c672a811debac8003048c10834)
* [Checkin URL](http://brightkite.com/objects/3cc3777472a811debdd7003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=18/1.31291/103.85379)


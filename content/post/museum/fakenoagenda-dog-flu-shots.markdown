---
title: "#FakeNoAgenda: Dog-flu shots"
date: "2013-11-25T02:28:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[Tanya Basu writing for National Geographic](http://news.nationalgeographic.com/news/2013/11/131115-flu-shot-dog-science-pets/):

> If your dog stays indoors and doesn't come into contact with neighborhood dogs, the vaccination isn't necessary. But if Spot's going to be at doggy day care, getting trained, heading to the vet—if she's going anywhere where a lot of animals congregate—it might be smart to vaccinate.

Good grief, now they want to mind control our pets?

This is clearly to install a beacon system that will allow the Federal Government of Gitmo Nation West to activate certain dogs to kill their owners. It'd be messier than a hot tub cover, but harder to trace.


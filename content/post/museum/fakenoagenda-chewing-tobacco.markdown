---
title: "#FakeNoAgenda: Chewing tobacco"
date: "2014-01-03T13:16:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[No Agenda #579](http://579.nashownotes.com/):

> But just, the whole process of putting that thing in your mouth... I just don't understand it.

Agreed, chewing tobacco makes absolutely no sense. Spitting it out is disgusting. But smoking, and exhaling it into the air for *everyone* to breathe... that's totally understandable. Banning smoking is just heavy handed nanny state-ism, *consarnit*!


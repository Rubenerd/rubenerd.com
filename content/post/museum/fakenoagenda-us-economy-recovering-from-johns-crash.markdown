---
title: "#FakeNoAgenda: US economy recovering from John's 2013 crash?"
date: "2013-11-25T02:18:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[The Associated Press](http://www.ctvnews.ca/business/obama-wants-to-shift-focus-to-u-s-economic-progress-1.1557085):

> In his weekly radio and Internet address, Obama is seeking to shift the focus away from negative headlines. He says jobs have been created, the auto industry is recovering and deficits are falling.

In light of shadow stats and boots on the ground, we beg to differ. John said the US economy would collapse in October 2013, and it did. Right?


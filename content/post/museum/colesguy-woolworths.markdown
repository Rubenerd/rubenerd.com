---
title: "#Colesguy Woolworths"
date: "2011-01-01T18:06:00+10:00"
year: "2011"
category: Museum
tag:
- from-colesguy
- memes
---
<p style="font-style:italic">This originally appeared on Colesguy, an esoteric blog about celebrity chef Curtis Stone and his incessant Coles supermarket ads. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2011/colesguy-woolworths.jpg" alt="ColesGuy: Saw you at Woolies" style="width:500px; height:400px" /></p>


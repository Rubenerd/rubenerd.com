---
title: "#Brightkite: Millenia Walk - Paulaner Brauhaus"
date: "2009-07-18T22:22:15-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.85773479938507%2C1.290736087921618%2C103.86095881462097%2C1.2947422882567174&amp;layer=mapnik"></iframe>

Checked into Millenia Walk - Paulaner Brauhaus (9 Raffles Boulevard #01-01 Time Square@ Millenia Walk, Singapore,, Singapore).

* [Location URL](http://brightkite.com/places/bd6fad10741b11de9d79003048c0801e)
* [Checkin URL](http://brightkite.com/objects/bd80f908741b11de9d79003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=18/1.29274/103.85935)


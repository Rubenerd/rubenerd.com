---
title: "#Brightkite: Tuas Checkpoint"
date: "2009-07-14T19:39:47-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.63434433937073%2C1.3470153750501515%2C103.63595634698868%2C1.3490184306751842&amp;layer=mapnik"></iframe>

Checked into 1.347943, 103.634563.

* [Location URL](http://brightkite.com/places/4f4754fc70e011deb12c003048c0801e)
* [Checkin URL](http://brightkite.com/objects/6110f4e070e011de8adf003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.34802/103.63515)


---
title: "#TripleAES: Hello, world"
date: "2013-11-14T21:34:00+10:00"
abstract: "From TripleAES, my short-lived side blog"
year: "2013"
category: Museum
tag:
- language
- words-that-must-die
- from-tripleaes
---
*This originally appeared in my side-blog TripleAES⚂.*

Behold, my new side weblog on the internets. I was going to roll my own from scratch, but Tumblr just works out of the box. This will be my mental dumping ground for all the things, including tech, news, anime and everything in between. Well, not everything, that would be impractical.

The title is a homage to our beloved but now sorely outdated DES cipher. If AES is so much better, we should use it in the same way ;).

Cheers!


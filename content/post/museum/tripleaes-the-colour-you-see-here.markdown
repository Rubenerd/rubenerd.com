---
title: "#TripleAES: The colour you see here"
date: "2013-11-14T22:03:00+10:00"
abstract: "From TripleAES, my short-lived side blog"
year: "2013"
category: Museum
tag:
- colour
- design
- from-tripleaes
---
*This originally appeared in my side-blog TripleAES⚂.*

>“The first recorded use of Teal as a color name in English was in 1917”

From [Wikipedia], so it has to be right.

[Wikipedia]: https://en.wikipedia.org/wiki/Teal


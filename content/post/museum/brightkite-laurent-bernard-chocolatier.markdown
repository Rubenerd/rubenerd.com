---
title: "#Brightkite: Laurent Bernard Chocolatier"
date: "2009-07-03T10:26:15-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.83983910083771%2C1.290467935063164%2C103.84145110845566%2C1.2924710362322387&amp;layer=mapnik"></iframe>

Checked into Laurent Bernard Chocolatier (80 Mohammed Sultan Road #01-11 Singapore, Singapore, Singapore).

* [Location URL](http://brightkite.com/places/3aceadb867ee11de8799003048c0801e)
* [Checkin URL](http://brightkite.com/objects/3adcf01267ee11de8799003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.29147/103.84065)


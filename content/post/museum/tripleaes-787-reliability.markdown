---
title: "#TripleAES: 787 reliability"
date: "2013-11-16T20:55:00+10:00"
abstract: "From TripleAES, my short-lived side blog"
year: "2013"
category: Museum
tag:
- avaiation
- boeing
- boeing-787
- japan
- news
- norway
- safety
- from-tripleaes
---
*This originally appeared in my side-blog TripleAES⚂.*

[Tim Hepher reporting for Reuters]\:

> Boeing meanwhile expects to turn the corner on reliability problems with the 787 Dreamliner within six months as a result of software changes, he said. Customers including Qatar Airways and Norwegian Air have complained about the plane's reliability.

Absent is a mention of ANA; they and their customers were the hardest hit.

[Tim Hepher reporting for Reuters]: http://www.reuters.com/article/2013/11/16/us-airshow-dubai-boeing-idUSBRE9AF03E20131116?feedType=RSS&amp;feedName=businessNews


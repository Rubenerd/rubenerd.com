---
title: "#FakeNoAgenda: It’s the lack of QLD flouride, duh"
date: "2013-11-24T11:32:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[Rhiannon Elston, reporting for SBS Australia](http://www.sbs.com.au/news/article/2013/11/16/emergency-services-brace-schoolies-onslaught):

> Some 28,000 schoolies will descend on the Queensland city during November, and more than 500 are expected to need emergency care. The top three expected injuries are alcohol intoxication, drug poisoning and cuts to hands and feet from broken glass.

Distraction! Queensland is the only state in Australia that doesn't fluoridate their water. Naturally, people fly in from overstate and get crazy once they're no longer under it's sedative effects.


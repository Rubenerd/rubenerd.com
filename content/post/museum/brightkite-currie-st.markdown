---
title: "#Brightkite: Currie St"
date: "2009-08-26T00:44:56-06:00"
location: Adelaide
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=138.58696103096008%2C-34.92647933406514%2C138.59018504619598%2C-34.923193799000316&amp;layer=mapnik"></iframe>

Checked into Currie St (Currie St, Adelaide SA 5000, Australia).

* [Location URL](http://brightkite.com/places/f58037d4920b11de9a14003048c10834)
* [Checkin URL](http://brightkite.com/objects/f7711b1c920b11debbeb003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=18/-34.92484/138.58857)


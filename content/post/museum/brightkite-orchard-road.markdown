---
title: "#Brightkite: Orchard Road"
date: "2009-07-11T21:52:11-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.8311380147934%2C1.303966714723679%2C103.83275002241135%2C1.3059698051979478&amp;layer=mapnik"></iframe>

Checked into Orchard Road (Orchard Rd, Singapore, Singapore, Singapore).

* [Location URL](http://brightkite.com/places/60df9a6c6e9711dea20b003048c0801e)
* [Checkin URL](http://brightkite.com/objects/610025166e9711dea20b003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.30497/103.83194)


---
title: "#Brightkite: PastaMania (Cineleisure)"
date: "2009-07-03T00:39:17-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.83565753698349%2C1.3004592914209219%2C103.83726954460144%2C1.3024623846847405&amp;layer=mapnik"></iframe>

Checked into PastaMania (Cineleisure) (8 Grange Rd, Orchard, Singapore 23, Singapore).

* [Location URL](http://brightkite.com/places/3b246cb4679c11de8724003048c10834)
* [Checkin URL](http://brightkite.com/objects/3b2f3900679c11de8724003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.30146/103.83646)


---
title: "#Brightkite: Paragon Shopping Centre"
date: "2009-07-07T19:48:39-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.8348314166069%2C1.3028324338685582%2C103.83644342422485%2C1.3048355252457735&amp;layer=mapnik"></iframe>

Checked into Paragon Shopping Centre (Orchard, Singapore).

* [Location URL](http://brightkite.com/places/75ac89486b6111dea4e0003048c10834)
* [Checkin URL](http://brightkite.com/objects/75bb44b06b6111dea4e0003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.30383/103.83564)


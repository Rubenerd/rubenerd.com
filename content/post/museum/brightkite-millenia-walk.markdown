---
title: "#Brightkite: Millenia Walk"
date: "2009-06-29T04:39:11-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.85227918624878%2C1.2766043939028335%2C103.86588335037231%2C1.3042134140382349&amp;layer=mapnik"></iframe>

Checked into Millenia Walk (9 Raffles Boulevard, Singapore, Singapore 03, Singapore).

* [Location URL](http://brightkite.com/places/37516bad32211dd9bec003048c10834)
* [Checkin URL](http://brightkite.com/objects/14ec40c2649911de9446003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=16/1.2904/103.8591)


---
title: "#TripleAES: Fibre optic considered faster than copper"
date: "2013-11-21T21:10:00+10:00"
abstract: "From TripleAES, my short-lived side blog"
category: Hardware
tag:
- australia
- politics
- telecommunications
- nbn
- from-tripleaes
---
*This originally appeared in my side-blog TripleAES⚂.*

[Brian Karlovsky writing for ARN]\:

> Fujitsu has demonstrated servers with thin fiber optics that will use lasers and light to transfer data, replacing the older and slower copper technology.

Shh! Don't say that so loud in this country.

> The light was used to transfer data between the systems. Light is considered a faster way to transfer data than copper wire.

Admittedly, that wouldn't be too hard. I'd consider transferring light through the air (or self-refracting containment device such as a fibre optic cable) would be rather faster than transferring physical copper wire. That stuff is heavy.

[Brian Karlovsky writing for ARN]: http://www.arnnet.com.au/article/532466/fujitsu_channels_light_laser_accelerate_data_transfer_/


---
title: "#FakeNoAgenda: What Gates, Buffet won’t tell you"
date: "2013-11-24T11:45:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[Quentin Fottrell has a column in Market Watch](http://www.marketwatch.com/story/10-things-billionaires-wont-tell-you-2013-11-15?link=sfmw_sm) explaining what billionaires apparently won't tell us. They didn't include the most obvious: **that they're lizard people**.

Since John stopped writing for them, they've really gone downhill. I'm glad they dedicated a segment to detailing exactly the circumstances surrounding his departure. I mean, if they weren't fully transparent, it'd be pretty hypocritical.


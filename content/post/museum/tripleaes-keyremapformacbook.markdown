---
title: "#TripleAES: KeyRemap4MacBook"
date: "2013-11-16T18:17:00+10:00"
abstract: "From TripleAES, my short-lived side blog"
year: "2013"
category: Museum
tag:
- keyboards
- unicomp
- macos
- mac-os-x
- os-x
- from-tripleaes
---
*This originally appeared in my side-blog TripleAES⚂.*

Mavericks does not persistently store the key mappings on my Model M clone. It's infuriating, and had me pondering a Mountain Lion downgrade.

Through some forum searching, I came across [KeyRemap4MacBook]. It's bewildering with its numerous duplicated options shoehorned into one window, but my Windows and Alt keys are now mapped to Option and Command.

[KeyRemap4MacBook]: https://pqrs.org/macosx/keyremap4macbook/index.html.en


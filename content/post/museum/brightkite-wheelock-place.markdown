---
title: "#Brightkite: Wheelock Place"
date: "2009-07-06T04:37:36-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.82985591888428%2C1.3036878372085483%2C103.83146792650223%2C1.3056909279049216&amp;layer=mapnik"></iframe>

Checked into Wheelock Place (501 Orchard Road, Singapore, Singapore 23, Singapore).

* [Location URL](http://brightkite.com/places/e0cd923ac8dc11dd9eb6003048c10834)
* [Checkin URL](http://brightkite.com/objects/51de5946a1911debcf8003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.30469/103.83066)


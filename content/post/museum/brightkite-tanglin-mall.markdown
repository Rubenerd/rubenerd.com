---
title: "#Brightkite: Tanglin Mall"
date: "2009-06-27T01:57:18-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.82314234972%2C1.3031032669324032%2C103.82389873266219%2C1.3065543748079576&amp;layer=mapnik"></iframe>

Checked into Tanglin Mall (163 Tanglin Road, Singapore, Singapore 24, Singapore).

* [Location URL](http://brightkite.com/places/519f5e78cd0411dd96a6003048c10834)
* [Checkin URL](http://brightkite.com/objects/229d759c62f011debd02003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=19/1.30483/103.82352)


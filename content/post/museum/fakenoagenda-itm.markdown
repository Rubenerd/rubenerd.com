---
title: "#FakeNoAgenda: ITM"
date: "2013-11-24T10:29:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

The [No Agenda](http://noagendashow.com) show is a long running podcast by Adam Curry and John C. Dvorak. Rather than be influenced by advertisers, their conjecture is dictated by their donors, dubbed producers.

This blog is an unofficial companion to this fine podcast and the [No Agenda News Network](http://noagendanewsnetwork.com). 

The header image is of *Enoshima Junko* from Danganronpa, drawn by [zy2386262 on Pixiv](http://www.pixiv.net/member_illust.php?mode=medium&illust_id=39686291). Because guns mean freedom, and she has killer legs. Speaking as a "TV executive" of course, which excuses us from everything!


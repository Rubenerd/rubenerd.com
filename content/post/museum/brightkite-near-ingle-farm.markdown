---
title: "#Brightkite: Near Ingle Farm"
date: "2009-08-31T05:54:48-06:00"
location: Adelaide
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=138.64645779132843%2C-34.831488886231895%2C138.64968180656433%2C-34.828199552164286&amp;layer=mapnik"></iframe>

Checked into near Ingle Farm, South Australia, Australia.

* [Location URL](http://brightkite.com/places/e32bb778950f11dea555003048c0801e)
* [Checkin URL](http://brightkite.com/objects/154655e0962511dea4d8003048c0801e)
* [View Larger Map](http://www.openstreetmap.org/#map=18/-34.82984/138.64807)


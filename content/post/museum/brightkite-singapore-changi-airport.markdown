---
title: "#Brightkite: Singapore Changi Airport"
date: "2009-07-09T09:06:11-06:00"
location: Singapore
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=103.93598556518555%2C1.290955973244459%2C104.03915405273438%2C1.4191511350319146&amp;layer=mapnik"></iframe>

Checked into Singapore Changi Airport (Civil Aviation Authority of Singapore, Singapore Changi Airport, PO Box 1, Singapore, 918141, Singapore).

* [Location URL](http://brightkite.com/places/9f3a92c6c9a11dea920003048c10834)
* [Checkin URL](http://brightkite.com/objects/a00acda6c9a11dea920003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=13/1.3551/103.9876)


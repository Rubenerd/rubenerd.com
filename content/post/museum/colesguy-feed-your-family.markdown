---
title: "#Colesguy: Feed your family"
date: "2010-11-18T19:54:00+10:00"
year: "2010"
category: Museum
tag:
- from-colesguy
- memes
---
<p style="font-style:italic">This originally appeared on Colesguy, an esoteric blog about celebrity chef Curtis Stone and his incessant Coles supermarket ads. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2010/colesguy-feed.jpg" alt="ColesGuy: Feed your family, to me!" style="width:500px; height:527px" /></p>


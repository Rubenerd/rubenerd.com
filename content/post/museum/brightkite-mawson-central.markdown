---
title: "#Brightkite: Mawson Central"
date: "2009-08-25T16:51:36-06:00"
location: Adelaide
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=138.61431419849396%2C-34.811350113384734%2C138.61753821372986%2C-34.80805997505792&amp;layer=mapnik"></iframe>

Checked into Mawson Central (University Parade, The Levels, SA 5095, Australia).

* [Location URL](http://brightkite.com/places/7c6270263fbb11de9369003048c0801e)
* [Checkin URL](http://brightkite.com/objects/d833aa7c91c911de9b04003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=18/-34.80971/138.61593")


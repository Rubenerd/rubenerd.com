---
title: "#Brightkite: Adelaide Airport (2009-08-27)"
date: "2009-08-27T19:35:50-06:00"
location: Adelaide
year: "2009"
category: Museum
tag:
- from-brightkite
---
<p style="font-style:italic">This checkin originally appeared on <a href="https://rubenerd.com/tag/from-brightkite/" title="View all posts imported from Brightkite">Brightkite</a>, one of the first geolocation social networks. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<iframe style="width:498px; height:373px; border:1px solid;" src="http://www.openstreetmap.org/export/embed.html?bbox=138.50506782531738%2C-34.97368062335606%2C138.5566520690918%2C-34.921126527452685&amp;layer=mapnik"></iframe>

Checked into Adelaide Airport SA (Adelaide Airport SA, Australia).

* [Location URL](http://brightkite.com/places/fd8f18026ea011dead38003048c0801e)
* [Checkin URL](http://brightkite.com/objects/1e12eec2937311dea8e7003048c10834)
* [View Larger Map](http://www.openstreetmap.org/#map=14/-34.9474/138.5309)


---
title: "#FakeNoAgenda: Palin’s nucular intelligence"
date: "2013-11-25T11:43:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[Jason Easly, quoting Sarah Palin](http://www.politicususa.com/2013/11/24/sarah-palin-humiliates-fox-news-knowing-nuclear-option.html) and her baffling response to a question about the nuclear option:

> As for this rule change that some people are calling the nucular option under Senate rules, you know, I guarantee this week, Thanksgiving dinner, people sitting around their tables, we’re not going to be talking about the president blessing this thwarting of the balance of power in Congress with new Senate rules called the nucular [sic] option.

I agree with Adam and John, there's no reason why people should be poking fun at Sarah Palin. She's an erudite, intelligent person who probably even knows what erudite means.


---
title: "#Twitpic: Farmer’s Union Iced Coffee now at Coles!"
date: "2011-10-04T19:57:00+10:00"
year: "2011"
category: Museum
tag:
- coffee
- from-twitpic
---
<p><img src="https://rubenerd.com/files/2011/colesfarmersunion.jpg" srcset="https://rubenerd.com/files/2011/colesfarmersunion.jpg 1x, https://rubenerd.com/files/2011/colesfarmersunion@2x.jpg 2x" alt="I am unreasonably happy about this!" style="width:500px; height:374px" /></p>


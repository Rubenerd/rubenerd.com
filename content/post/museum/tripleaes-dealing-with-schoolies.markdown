---
title: "#TripleAES: Dealing with schoolies"
date: "2013-11-16T19:22:00+10:00"
abstract: "From TripleAES, my short-lived side blog"
year: "2013"
category: Museum
tag:
- australia
- news
- school
- from-tripleaes
---
**This originally appeared in my side-blog TripleAES⚂.

[Rhiannon Elston, reporting for SBS]\:

> Some 28,000 schoolies will descend on the Queensland city during November, and more than 500 are expected to need emergency care. The top three expected injuries are alcohol intoxication, drug poisoning and cuts to hands and feet from broken glass.

Idiots.

[Rhiannon Elston, reporting for SBS]: http://www.sbs.com.au/news/article/2013/11/16/emergency-services-brace-schoolies-onslaught


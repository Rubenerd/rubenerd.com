---
title: "#FakeNoAgenda: Space Operations conference"
date: "2013-12-10T12:31:00+10:00"
year: "2013"
category: Museum
tag:
- from-fakenoagenda
---
<p style="font-style:italic;">This originally appeared on <a href="https://rubenerd.com/tag/from-fakenoagenda/">FakeNoAgenda</a>, a parody site about the podcast a few fans and I ran. It’s <a title="View all posts in the museum" href="https://rubenerd.com/museum/">preserved here</a> here for posterity.</p>

<p><img src="https://rubenerd.com/files/2013/fakenoagenda.jpg" srcset="https://rubenerd.com/files/2013/fakenoagenda.jpg 1x, https://rubenerd.com/files/2013/fakenoagenda@2x.jpg 2x" alt="Fake No Agenda, art by Enoshima Junko from Pixiv as seen in first FakeNoAgenda post" style="width:500px; height:240px" /></p>

[Via Twitter](https://twitter.com/SpaceRef/status/410037449906475008):

> AIAA SpaceOps 2014 International Conference on Space Operations. Registration is open. #spaceops2014 http://t.co/8FRdOUfTyL

This is clearly a conference for elites to organise which moon bases they'll have their penthouses in.


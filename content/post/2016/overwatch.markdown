---
draft: true
title: "Overwatch"
year: "2016"
location: Sydney
category: software
tag:
- games
---
The character designs, graphics and scene detail in *Overwatch* look amazing, and its already spawned some serious cosplayers. But what exactly is it?

> Overwatch is a multiplayer first-person shooter video game [..]

Ugh, nevermind.


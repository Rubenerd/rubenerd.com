---
title: "Folding paper seven times"
date: "2016-03-16T16:01:00+10:00"
abstract: "Or when you turn paper to wood"
year: "2016"
location: Sydney
category: Hardware
tag:
- paper
- science
- video
---
<p></p>

<iframe width="500" height="281" src="https://www.youtube.com/embed/KuG_CeEZV6w" frameborder="0" allowfullscreen></iframe>

A coworker and I were discussing Netflix, and how he didn't even need it now given all the weird and wonderful stuff on YouTube. I love serialised stuff (and anime!) too much to give up streaming services, but there are certainly some gems out there.

Via <a href="https://twitter.com/alanjlee">@AlanJLee</a> and; judging from the insipid comments; Reddit.


---
title: "Ground Control to Major Tom"
date: "2016-01-12T11:54:00+10:00"
abstract: "Goodbye David Bowie"
year: "2016"
location: Sydney
category: Media
tag:
- david-bowie
- music
- rip
---
<p><iframe width="500" height="315" src="//www.youtube.com/embed/9G4jnaznUoQ" ></iframe></p>

I thought [this tweet][tweet] was the best:

> @OhNoSheTwitnt: Listening to Under Pressure and imagining David
> and Freddie performing it together in Heaven today and I don't 
> even believe in Heaven.

Personally, [this music video][musicvideo] will remain my favourite of all time. I wasn't a rabbid *Ziggy Stardust* fan, but he sure had his moments.

RIP you glorious gentleman.

[tweet]: https://twitter.com/OhNoSheTwitnt/status/686525492959285248
[musicvideo]: https://rubenerd.com/bowie-and-jagger-dancing-in-the-street/


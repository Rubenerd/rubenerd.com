---
title: "Facebook privacy"
date: "2016-05-20T08:22:00+10:00"
---
I logged into my profile for the first time in a few weeks.

> Ruben, want to do a Privacy Check-up?
> 
> **We care about your privacy** and want to make sure that you're sharing with the right people. Here's a quick way to review the privacy of your posts, apps and some profile info.
> 
> The Facebook Privacy Team

Thanks, I needed a laugh this morning :).


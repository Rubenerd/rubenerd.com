---
title: "Won't Get Fooled Again"
date: "2016-04-01T11:46:00+10:00"
abstract: "Happy April Fools"
year: "2016"
location: Sydney
category: Media
tag:
- music
- the-who
---
<p></p>

<iframe width="500" height="375" src="https://www.youtube.com/embed/SHhrZgojY1Q"></iframe>

Happy April Fools! No cringe-worthy jokes or Windows Vista reviews this year, just one of the greatest rock songs of all time (and I didn't even [sing it myself] this time). Enjoy!

[sing it myself]: https://rubenerd.com/show295/ "Rubénerd Show 295: The tangentially Who episode"


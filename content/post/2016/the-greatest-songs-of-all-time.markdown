---
title: "The greatest songs of all time"
date: "2016-11-07T07:43:00+10:00"
year: "2016"
location: Sydney
category: Media
tag:
- lists
- music
---
<p></p>

<iframe src="https://www.youtube.com/embed/eS3AZ12xf6s" style="border:0px; width:500px; height:280px"></iframe>

This is not to be confused with the *best songs of all time*, to be published later. It's also subject to review and addition.

1. In Search of the Perfect Shampoo, Michael Franks
2. The Bird is The Word, The Trashmen
3. Macarron Chacarron, El Chombo
4. Cobrastyle, The Teddybears
5. Cool for Cats, Squeeze
6. Ca Plane Pour Moi, Plastic Bertrand
7. Gangnam Style, PSY
8. Open the Door, Im Chang Jung
9. Frontier Psychiatrist, The Avalanches
10. Don't Cry for No Hipster, Ben Sidran
11. Albuquerque, "Weird Al" Yankovic
12. I Don't Ski, The Renovators
13. PONPONPON, Kyary Pamyu Pamyu


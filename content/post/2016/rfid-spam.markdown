---
draft: true
title: "RFID spam"
---
Fastmail is generally pretty good with spam, but an oddly-specific source of the nefarious junk has been filtering through. It started with this in early March:

> Dear Sir/Madam, How are you? We are chengdu Mind, a professional RFID card/NFC tag/contact IC card etc manufacturer over 20 years from China, Our market is mainly iUSA,England,Germany,Singapore etc.Our clients includes Microsoft,Samsung,Benzing etc. We have our own factory and we are 100% manufacturer. Hoping we will have a chance of cooperation.pls contact us if any interest. Waiting for your kindly reply. 

iUSA! It's USA, but with better industrial design.

> Dear Sir/Madam, I know your email on internet, and found you have demand for Access control card, you can send us an inquiry to test our service:-) Best regards! 

I have demand for email access control, clearly.



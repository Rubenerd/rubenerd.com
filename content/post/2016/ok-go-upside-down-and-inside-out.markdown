---
title: "OK Go: Upside Down & Inside Out"
date: "2016-05-12T10:30:00+10:00"
year: "2016"
location: Sydney
category: Media
tag:
- colour
- music
- music-videos
---
This is one of the [greatest music videos] I’ve ever seen.

<iframe width="500" height="275" src="https://www.youtube.com/embed/LWGJA9i18Co"></iframe>

Explaining [how they did it] was even better.

<iframe width="500" height="275" src="https://www.youtube.com/embed/pnTqZ68fI7Q"></iframe>

<p></p>

[greatest music videos]: https://www.youtube.com/watch?v=LWGJA9i18Co
[how they did it]: https://www.youtube.com/watch?v=pnTqZ68fi7Q


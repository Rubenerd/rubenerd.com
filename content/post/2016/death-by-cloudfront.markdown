---
title: "Death by CloudFront"
date: "2016-02-03T15:50:00+11:00"
abstract: "So many identifiers..."
year: "2016"
location: Sydney
category: Internet
tag:
- amazon
- cdns
---
<p><img src="https://rubenerd.com/files/2016/screenie.deathbycloudfront.png" alt="Screenshot showing four distinct, random CloudFront end points in NoScript that all have to be enabled" style="width:424px; height:462px;" /></p>


---
layout: "page"
title: "Design"
---
The word “taste” is painfully paraded around as much as "[nuance](https://rubenerd.com/show294/)" in 2015 tech circles. Some people have it, others aspire to it, and all lament the lack of it in others.

Since 2010, I aimed to make *Rubénerd* more contemporary, tasteful, classy. What the cool people do. I switched to a [blazing white](https://en.wikipedia.org/wiki/Light-on-dark_color_scheme#.22Blazing_white.22) theme, tried centering stuff, rendered thinner fonts, abstained from all images, and generally made everything look cookie-cutter, generic, and forgettable.

Then I realised something. *There are enough of those sites out there*. I'd make my own corner of the net as unique as dishwater.

Screw that.

So in the end, I reverted to my teenage-designed, 2005 colour scheme. Is it contemporary, tasteful and classy? Heck no. *But it's mine*.

It also presents tangible benefits. I can look at it with my excessive eye floaters. I find it soothing on my overworked eyes. It's less of a shock going from dark Vim terminal windows to it. I love how smoothly OS X renders light fonts.

I've also never loaded heavy JavaScript libraries or external fonts, and I'm not about to. That's my definition of taste.

Cheers.


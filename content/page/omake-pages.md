---
layout: page
title: "Omake"
---
*Omake* is an oft-abused [Japanese term] used for bonus or extra content. It's likely what most people would put on a personal homepage, rather than a blog. Maybe there's something here that will be interesting for you :). 

### Lists 📝

* [Charities and non-profits](/omake/charities/) that I donate to, and you should check out
* [Engine room](/omake/engine-room/) of stuff that powers *Rubenerd*
* [Friend blogroll](/omake/blogroll/) of people you should be reading
* [Search engines](/omake/search-engines) all on one page

### Travel ✈️

* The best [coffee](/omake/coffee/) around the world
* My [favourite buildings](/omake/favourite-buildings) Clara and I are ticking off


### Collections 💾

* [CD-ROM archive](/omake/cdrom-archive), outsourcing my collection to Archive.org
* [The fleet](/omake/the-fleet/), my current computers
* [eBook manga](/omake/ebook-manga/), availabilty to replace my dead trees 
* [People using my photos](/omake/people-using-my-photos/), mostly from Creative Commons
* [Plugins](/omake/plugins/), for Firefox and Safari
* [Recommended](/omake/recommended/), books, manga, podcasts, maybe eventually more


### Computers 🖥

* [NAS computer cases](/omake/nas-cases/), research for my consolidated NAS
* [Mini-ITX](/omake/mini-itx), matrix of the best tiny, space saving cases
* [Mizuno on PCPartPicker](https://pcpartpicker.com/user/rubenerd/saved/3n3qqs), restoring my first computer from 1998
* [Tsuruya on PCPartPicker](https://pcpartpicker.com/user/rubenerd/saved/4yc323), my budget game machine build


### Other projects 🎷

* [WikiProject Jazz](/omake/wikiproject-jazz/), on Wikipedia

[Japanese term]: https://en.wiktionary.org/wiki/omake


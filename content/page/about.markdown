---
layout: page
title: "About"
---
<img src="https://rubenerd.com/files/2017/me.jpg" alt="Me!" style="float:right; margin:0 0 1em 2em; height:128px; width:128px;" />

* [Abstract](#abstract)
* [About the site](#site)
* [About me](#me)
* [Contact](#contact)
* [About Rubi, the site mascot](#mascot)
* [Geek code](#geekcode)

### Abstract

I've always been struck—ouch—at the futility of employing washroom hand dryers. Other than those *Airblade* contraptions that sonically wash your eardrums, you press a button only to find nothing happens beyond a semblance of mildly warm air hitting your responsibly-soaked hands.

So you hit up the paper towel dispenser, only to discover there isn't anywhere convenient to discard the used portion. Kubernetes. Abstract enough for you?


<h3 id="site">About the site</h3>

*Rubenerd* is my weblog and [podcast] of such observations. An *artisanal blogging expert* once opined blogs should be limited to a single topic; so I've endeavoured not to since.

The design is an intentional throwback to a time when blogging was fun, and held the promise of being a loose-nit federation of writers, before social networks sucked... the oxygen out of the room.

It's proudly powered by FreeBSD, the world's greatest operating system. More information can be found on the [Engine Room Omake] page.

[Engine Room Omake]: https://rubenerd.com/omake/engine-room/


<h3 id="about-me">About me</h3>

I'm Ruben Schade, and I'm relatively harmless. I have a lovely girlfriend [Clara] with whom I spend far too much time drinking bubble tea and [travelling]. I love amatear photography, vintage computing, and fleshing-out about pages.

I make a living as a technical writer and systems architect for an IaaS company in Sydney and San Francisco, but I grew up in Singapore where I developed a taste for well-funded infrastructure.

[podcast]: https://rubenerd.com/show/
[travel]: https://instagram.com/rubenschade/


<h3 id="contact">Contact me</h3>

The easiest way is via [Twitter], which for better or worse I still use. Otherwise, this is my email, which can be used with my [PGP public key] if so desired:

    printf "%s@%s.%s" me rubenschade com

[Twitter]: https://twitter.com/rubenerd
[PGP public key]: https://pgp.mit.edu/pks/lookup?op=vindex&search=0x9CFC8AEBBD528543

I'm also on [Pinboard], [GitHub], [GitHub Gists], [Flickr], [Instagram], [Wikipedia], [YouTube], and [Vimeo]. I have [Facebook], but don't use.

[Pinboard]: https://pinboard.in/u:Rubenerd
[GitHub]: https://github.com/rubenerd
[GitHub Gists]: https://gist.github.com/rubenerd
[Flickr]: https://flickr.com/photos/rubenerd
[Instagram]: https://instagram.com/RubenSchade
[Wikipedia]: https://en.wikipedia.org/wiki/User:RubenSchade
[Facebook]: https://facebook.com/ruben.schade
[YouTube]: https://www.youtube.com/user/rubenerd
[Vimeo]: https://vimeo.com/rubenerd


<h3 id="mascot">About Rubi, the site mascot</h3>

*Rubi* is the site's mascot, drawn by the aforementioned Clara. Her mismatching, Miku-esque boots elude to my predilection for not wearing matching socks.

Funny story, I was chided for wearing orange and red socks during a high school work experience day, with the implication that I wasn't taking the assignment seriously. The day I'm looked over for hiring because of my socks is the day I miss a bullet.


### Geek code

Do people still publish these?

    -----BEGIN GEEK CODE BLOCK-----
    Version: 3.1
    GIT/TW d+ s+:-- a C+++ UBL++++$ P+++>++++ L+ E--- 
    W+++ N+ !o? K-? w--$ O+ M++$ V PS+ PE- Y+ PGP+ t++ 
    5+ X+ R tv b++ DI-- D G+ e++ h r++>+++ y+
    ------END GEEK CODE BLOCK------ 


[Clara]: http://kiri.sasara.moe/
[podcast]: https://rubenerd.com/show/
[travelling]: https://instagram.com/RubenSchade


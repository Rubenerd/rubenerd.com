---
layout: page
title: "Music"
---
Latin jazz and big band were my first loves, but I'm also the product of my parents. Michael Franks remains my favourite singer/songwriter, in part for his beautiful melodies and witty lyrics.

## Concerts I’ve been to
The great thing about people who disapprove of the following is that I can ignore you.

* Chick Corea and Gary Burton
* Sugar Ray, Shaggy, Cindy Lauper, Pet Shop Boys @ SingFest
* Train
* Maroon 5
* Bebel Gilberto
* Sonny Rollins
* Weird Al Yankovic
* Coldplay
* Jack Johnson
* Jimmy Eat World
* PSY
* Kyary Pamyu Pamyu
* Santana and the Moody Blues


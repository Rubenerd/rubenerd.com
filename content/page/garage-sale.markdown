---
layout: page
title: "Garage Sale"
---
I'm giving away and selling most of my stuff. It's painful, but necessary.

Old books and computer gear likely wouldn’t be accepted anywhere, so if anyone else can find value before they get sent to recycling, that'd be wonderful.

### Lists of stuff to sell (so far)

* [Books and manga]
* [Computer parts and peripherals]

### Shipping items

Email me at [garagesale@rubenschade.com] with any items you want and your address, and I'll send you a PayPal shipping invoice. If you want to throw a few extra bucks at me too, I'll happily take :).

It'd probably only make sense shipping them around Australia, but if you're overseas and badly want any of it, I'll see what I can do.

### Meeting for items

I'm happy to meet at the [Mascot train station] \(two stops from Sydney Central) to give you stuff if you're near by. Email me and we'll arrange.

<iframe width="500" height="320" src="http://www.openstreetmap.org/export/embed.html?bbox=151.17985725402832%2C-33.93139678750912%2C151.18951320648193%2C-33.91489127850173&amp;layer=mapnik" style="border:0px;"></iframe>

Thanks.

[books and manga]: https://www.goodreads.com/review/list/785997-ruben-schade?utf8=✓&shelf=giving-away&per_page=100&view=covers

[computer parts and peripherals]: /garage-sale-parts/

[Mascot train station]: http://www.openstreetmap.org/#map=16/-33.9231/151.1847

[garagesale@rubenschade.com]: mailto:garagesale@rubenschade.com

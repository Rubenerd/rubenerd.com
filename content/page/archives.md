---
layout: page
title: "Archives"
---
*Rubenerd* has more than 5,000 posts and podcast entries spread over 13+ years. This is how you can view them all, somewhat less tediously, than using the pagination links on the home page.


<h3 id="category">Posts by main category</h3>

* [Anime](/anime/), now rarely added to, but I used to be a good little weeb
* [Hardware](/hardware/), computers, servers, phones, jaffle irons
* [Internet](/internet/), websites, online privacy and security, standards
* [Media](/media/), photography, journalism, music
* [Software](/software/), mostly macOS, FreeBSD, (GNU/)Linux
* [Thoughts](/thoughts/), my catch–all for other random stuff


<h3 id="other-categories">Posts by other category</h3>

* [Museum](/museum/), posts imported from retired/dead services
* [Pinboard](https://pinboard.in/u:Rubenerd), my external link blog, previously [del.icio.us](https://del.icio.us/rubenerd)
* [Rubenerd Show](/show/), my long-running podcast of nerdish observations
* [Unsub Me Already](http://unsub.rubenerd.com/), a Tumblr chronicling email (mis)adventures


<h3 id="location">Posts by tag</h3>

There are far too many to list here. But if you see a recurring topic, chances are there's a tag. For example, here's [FreeBSD], [travel], and [privacy].

[FreeBSD]: https://rubenerd.com/tag/freebsd/
[travel]: https://rubenerd.com/tag/travel/
[privacy]: https://rubenerd.com/tag/privacy/


<h3 id="location">Posts by location</h3>

I only started putting locations on posts in 2018. I'll get around to retroactively adding them one day.

* [Los Angeles](/location/los-angeles/)
* [Oakland](/location/oakland/)
* [San Francisco](/location/san-francisco/)
* [Sydney](/location/sydney/)


<h3 id="year">Posts by year</h3>

* [2018](/year/2018/)
* [2017](/year/2017/)
* [2016](/year/2016/)
* [2015](/year/2015/)
* [2014](/year/2014/)
* [2013](/year/2013/)
* [2012](/year/2012/)
* [2011](/year/2011/)
* [2010](/year/2010/)
* [2009](/year/2009/)
* [2008](/year/2008/)
* [2007](/year/2007/)
* [2006](/year/2006/)
* [2005](/year/2005/)
* [2004](/year/2004/)


---
layout: page
title: "Garage Sale - Parts"
---
[&larr; Back to Garage Sale page](/garage-sale/)

### Working, with cables and power bricks
* Epson GT7000 SCSI 4800DPI scanner
* Parallel Port PCI card
* 250MB USB Zip Drive

### Unsure
* 100MB parallel port Zip Drive, broken top case
* 100MB USB Zip Drive, good condition but Mac doesn't recognise

### Dead
Dead computer components are being recycled. If you badly want to know what I have, let me know. It's mostly dead memory and other pointless stuff.

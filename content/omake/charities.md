---
layout: omake
title: "Charities and non-profits"
---
The [diffusion of responsibility] problem means most of us don’t donate to worthy causes, because we don’t think it’ll make a difference. This will only change when we go against this perverse intuition.

These are the charities and non-profit organisations I donate money to, and encourage you to as well.

### Contents

* [Health and research](#health)
* [Software projects](#software)
* [Websites](#websites)
* [Media](#media)
* [Policy](#policy)



<h3 id="health">Health and research</h3>

<dl>
<dt style="font-weight:bold"><a href="https://curecancer.org/">Cure Cancer Australia</a></dt>

<dd><p>This organisation directly funds cancer researchers, and without resorting to employing clipboard pushers in public places to intimidate people into donations. It's a personal issue for me, having lost several family members including my mum to the disease.</p></dd>


<dt style="font-weight:bold"><a href="http://globaldevelopmentgroup.org/au/">Global Development Group</a></dt>

<dd><p>This is the local Australian arm of the World Food Programme. They disseminate food to needy people after emergencies and conflict, and help rebuild the lives of those affected. You can choose which areas to donate to, or let them decide who needs it most urgently each month. If you’re outside Australia, check out the <a href="https://wfp.org">World Food Programme</a> website to see where you can donate.</p></dd>


<dt style="font-weight:bold"><a href="https://www.guidedogs.com.au/">Guide Dogs NSW</a></dt>

<dd><p>I only learned recently that most vision-impaired people can’t afford guide dogs, and the government chips in bugger all to help them out. Guide Dogs NSW provides guide dogs, mobility services, training, and orientation for those affected by impaired vision.</p></dd>

</dl>



<h3 id="software">Software projects</h3>

<dl>

<dt style="font-weight:bold"><a href="https://freebsdfoundation.org">FreeBSD Foundation</a> </dt>

<dd><p>FreeBSD is my first operating system of choice for personal projects, and professionally for all manner of servers. I was super fortunate to meet its vice president Benedict Reuschling and some of the devs at AsiaBSDCon 2018; they’re wonderful people.</p> 

<p>The Foundation supports the FreeBSD project, development, and community. It sponsors developers and events, and provides travel grants for those who can’t afford to attend developer summits.</p></dd>


<dt style="font-weight:bold"><a href="https://ostif.org/donate-to-ostif/">Open Source Technology Improvement Fund</a></dt>

<dd><p>It's hard to overstate how critically important the technologies this fund supports are. OpenSSL is the backbone of the secure internet, OpenVPN is the de-facto small VPN standard, GnuPG secures email, and VeraCrypt is a viable, more secure disk encryption system, especially for Windows users.</p>

<p>My livelihood and personal life are made possible by these technologies, and I suspect yours are too.</p></dd>

</dl>


<h3 id="websites">Websites</h3>

<dl>

<dt style="font-weight:bold"><a href="https://archive.org/donate/">Internet Archive</a></dt>

<dd><p>This is one of the Internet’s greatest treasures. The Internet Archive is a non-profit public digital library with content accessible to everyone. Their Wayback Machine stores snapshots of websites in time, many of which no longer exist. They’ve saved thousands of broken links on Wikipedia.</p>

<p>I also feel a personal affinity to them, given they’ve graciously hosted the files and bandwidth for every episode of my podcast, and the Overnightscape Underground network, for free for more than a decade. We could not have become podcasters without the Internet Archive, it’s as simple as that.</p></dd>

<dt style="font-weight:bold"><a href="https://wikimediafoundation.org/wiki/Ways_to_Give">Wikimedia Foundation</a></dt>

<dd><p>When I think <em>free access to the world’s knowledge</em>, I don’t think of a certain search engine, I think of Wikipedia and its related projects. I’ve been a reader and contributor since at least 2004, and couldn’t imagine beginning any research without it.</p>

<p>These sites regularly rank among huge commercial operations in search results and traffic, many of which freeload off them, yet they receive a fraction of the income. If we want to keep the site ad free, it’s up to us to fund it.</dd>

</dl>


<h3 id="media">Media</h3>

<dl>

<dt style="font-weight:bold"><a href="https://www.youtube.com/channel/UCJ0-OtVpF0wOKEqT2Z1HEtA">ElectroBOOM</a></dt>

<dd><p>Medhi has the best channel on YouTube; his videos are as hilarious as they are educational. I’ve learned more watching his electronics videos than anyone else, and want him to continue doing it full time now :).</p></dd>


<dt style="font-weight:bold"><a href="https://samharris.org/">SamHarris.org</a></dt>

<dd><p>The only antidote to bad ideas are good ideas and conversations. I’m sometimes at-odds with Sam’s guests, but they always make me think. His books on spirituality without religion, and how to apply critical thinking to meditation, have also both had a profound effect on me.</p></dd>


<dt style="font-weight:bold"><a href="http://www.wellmaywesay.com/">Well May We Say</a></dt>

<dd><p>A political podcast by Jeremy Sear of <em>Something Wonky</em> fame. He’s a voice of compassion and reason in an Australian media landscape increasingly devoid of either.</p></dd>

</dl>



<h3 id="policy">Policy</h3>

<dl>

<dt style="font-weight:bold"><a href="https://thegreens.org.au/">The Australian Greens</a></dt>

<dd><p>I’ve been a Greens voter since I could be, with preferences flowing after that to whoever I deem reasonable. I don’t agree with all their policy positions; for example we should still be considering nuclear power if the primary tool to tackle climate change is meaningful and immediate cuts to carbon emissions.</p>

<p>But they’re the only major political party in Australia with any digital literacy, voting against the Coalition’s mandatory data retention, and Labor’s internet filter. They’re also the only major party against Australia’s illegal immigration camps. These aren’t radical positions, they’re just ethically and financially sensible.</p></dd>



</dl>


[diffusion of responsibility]: https://en.wikipedia.org/wiki/Diffusion_of_responsibility 

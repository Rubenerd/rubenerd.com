---
layout: omake
title: "Wallet iPhone 7+ Cases"
---
My iPhone 7+ is already the size of a dinner tray, so may as well have it serve as my wallet too. Requirements I could think of:

* 3-4 card slits
* Cash pounch
* Kick stand, for my foldable keyboard
* Not real leather, for reasons
* Kitchen sink

Case           | Material | Cards | Colours   | Closing
---------------|----------|-------|-----------|------------
[IPHOX]        | PU       | 2     | ◼️ 🏾      | Magnet front
[Magic Button] | PU       | 3     | ◼️ 🏾 ◻️ 🌈 | Magnet clasp
[TECOOL]       | PU       | 3     | ◼️ 🏾      | Magnet clasp
[TUCCH]        | PU       | 3     | ◼️ 🏾      | Magnet clasp

[IPHOX]: http://www.ebay.com.au/itm/GENUINE-IPHOX-Magnetic-PU-Leather-Wallet-Flip-Case-Cover-iPhone-7-6-S-Plus-5-SE-/182155317379
[Magic Button]: http://www.ebay.com.au/itm/New-Premium-Flip-Wallet-Case-PU-Leather-Card-Slot-Cover-For-iPhone-6s-7-Plus-/282417493224
[TECOOL]: https://www.amazon.com/iPhone-TECOOL-Leather-Function-Magnetic/dp/B01N0V75P2/
[TUCCH]: https://www.amazon.com/TUCCH-Carry-All-Magnetic-Closure-Leather/dp/B01KO14FDM/


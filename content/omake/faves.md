---
layout: omake
title: "Favourite things"
---
Without any brown paper packages tied up with string, this is a pointless, incomplete, work-in-progress table of my favourite things.

Favourite                    | What
-----------------------------|--------------
Graphic designer             | [Saul Bass](https://commons.wikimedia.org/wiki/File:Saul_Bass_logos_compilation.jpg)
Name that sounds like a fish | [Saul Bass](https://commons.wikimedia.org/wiki/File:Saul_Bass_logos_compilation.jpg)
Jazz singer/songwriter       | [Michael Franks](http://michaelfranks.com/)
Underground                  | [Hong Kong MTR](http://www.mtr.com.hk/en/customer/tourist/index.php)
Anime                        | [Barakamon](https://myanimelist.net/anime/22789/Barakamon)
Long-running manga           | [Fairy Tail](https://global.bookwalker.jp/series/89114/)
Anime fashion trend          | [Zettai ryouiki](http://www.projectharuhi.net/?p=10350)
Fate/Grand Order servant     | [Mashu](https://grandorder.wiki/Mash_Kyrielight)
Visited city                 | [Kyoto](http://www.city.kyoto.lg.jp/)
Visited building             | [Chrysler Building](https://www.jamesmaherphotography.com/new-york-historical-articles/chrysler-building/)
Car                          | [Auburn 851 Speedster](https://en.wikipedia.org/wiki/Auburn_Speedster)
Shopping centre              | [Namba Parks](http://www.nambaparks.com.e.uq.hp.transer.com/)
American coast               | [Northeastern](https://en.wikipedia.org/wiki/Northeastern_United_States)


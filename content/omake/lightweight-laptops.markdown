---
layout: omake
title: "Lightweight laptops"
---
I'll find one eventually!

Laptop                         | Weight | Resolution
-------------------------------|--------|-----------------
[GPD Pocket]                   | 0.480  | 7" 1920×1200
[Panasonic Let's Note CF-RX6A] | 0.745  | 11.5" 1920×1200
[Apple MacBook One]            | 0.92   | 12" 2304×1440 
[Toshiba Portage X20]          | 1.1    | 12.5" 1920×1200
[HP EliteBook x360 1020]       | 1.13   | 12.5" 3840×2160
[Panasonic Let's Note CF-AX2]  | 1.15   | 11.3" 1920×1200
[Toshiba Portage X30]          | 1.05   | 13.3" 1920×1080
[ThinkPad X1 Carbon 5]         | 1.14   | 14" 1920×1080
[Toshiba Portage Z30]          | 1.20   | 13.3" 1366×768
[Dell XPS 13 2-in-1]           | 1.24   | 13.3" 3200×1800
[ASUS ZenBook UX430UA]         | 1.25   | 14" 1920×1080

[GPD Pocket]: https://www.indiegogo.com/projects/gpd-pocket-7-0-umpc-laptop-ubuntu-or-win-10-os-laptop--2#/
[Dell XPS 13 2-in-1]: http://www.dell.com/au/business/p/xps-13-9365-2-in-1-laptop/pd?oc=z521202au&model_id=xps-13-9365-2-in-1-laptop&l=en&s=bsd
[Toshiba Portage X20]: http://www.mytoshiba.com.au/products/computers/portege/x20
[Toshiba Portage X30]: http://www.mytoshiba.com.au/products/computers/portege/x30
[Toshiba Portage Z30]: http://www.mytoshiba.com.au/products/computers/portege/z30
[Apple MacBook One]: https://www.apple.com/macbook/
[ASUS ZenBook UX430UA]: https://www.asus.com/au/Laptops/ASUS-ZenBook-UX430UA/
[Panasonic Let's Note CF-RX6A]: https://panasonic.jp/cns/pc/products/rz6a/
[Panasonic Let's Note CF-AX2]: http://www.todroid.com/panasonic-ax-series-windows-8-ultrabook-hybrid-unveiled-at-ceatec-2012-in-japan/
[ThinkPad X1 Carbon 5]: http://www3.lenovo.com/au/en/deals/current-offers/deals-of-the-week/NoteBook-TP-X1-C5-8G-256-W10H/p/20HR001SAU?ipromoID=auow_pub_rh1_carbon5
[HP EliteBook x360 1020]: http://www8.hp.com/us/en/ads/elite/elitebook-x360-1020.html


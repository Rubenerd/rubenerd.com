---
layout: omake
title: "Mini-ITX"
---
I wanted to build the smallest gaming machine I could. I ended up going with the NCASE M1, but am looking to downscale. Needs to support:

* Graphics cards up to 295mm long
* SFX power supply
* 2x SSDs (2x 2.5, or 1x M.2 + 1x 2.5)
* Minimal design
* Not black

### Crowdfunded cases

Case         | Size (H x W x D)  | Volume | Colour
-------------|-------------------|--------|--------
[Sentry ITX] | 340 x 310 x 66mm  | 6.9L   | White
[Dan A4-SFX] | 205 x 112 x 327mm | 7.2L   | Silver
[NCASE M1]   | 250 x 160 x 338mm | 12.6L  | Silver

[Sentry ITX]: http://zaber.com.pl/sentry/
[Dan A4-SFX]: https://www.dan-cases.com/dana4.php
[NCASE M1]: https://www.ncases.com

### Other cases

Case                       | Size              | Volume
---------------------------|-------------------|--------
[Fractal Design Node 202]  | 377 x 88 x 332mm  | 10.2L
[SilverStone RAVEN RVZ01B] | 382 x 105 x 350mm | ?
[SilverStone Milo ML08B]   | 87 x 380 x 370mm  | 12L
[Lian Li PC-Q19]           | 491 x 160 x 286mm | 12.7L

[Fractal Design Node 202]: https://www.newegg.com/Product/Product.aspx?Item=N82E16811352058&cm_re=mini_itx_case-_-11-352-058-_-Product
[SilverStone RAVEN RVZ01B]: https://www.newegg.com/Product/Product.aspx?Item=N82E16811163252
[SilverStone Milo ML08B]: https://www.newegg.com/Product/Product.aspx?Item=N82E16811163286
[Lian Li PC-Q19]: http://www.lian-li.com/en/dt_portfolio/pc-q19/


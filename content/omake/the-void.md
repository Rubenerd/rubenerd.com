---
layout: omake
title: "The Void"
---
I created this page in March 2005 for my then-student site. I've resurrected it here in all it's glory. Thank you.

<!--

----------------------------------------------------------

 "Ha! I laugh at danger and drop ice cubes down the vest 
 of fear!" 
    
 -- Edmund Blackadder

----------------------------------------------------------

 "When did ignorance become a point of view?"
    
 -- Dilbert

----------------------------------------------------------

 "The power of accurate observation is often called
 cynicism by those who don't have it."
    
 -- G.B. Shaw

----------------------------------------------------------

 "If you pick up a stray dog
 and make it prosperous
 he will not bite you
 this is the principal difference
 between a dog and a man.
 
 -- Mark Twain

----------------------------------------------------------

 "Cheer up! The worst is yet to come!"
    
 -- Philander Johnson

----------------------------------------------------------

 "He who knows not, knows not
  He who knows he knows not, knows."

 -- Mr X

----------------------------------------------------------

 "He that hopes no good feels no ill."

 -- Thomas Fuller

----------------------------------------------------------
 
 "The fact that no one understands you doesn't mean
  you're an artist."

 -- MacEddie

----------------------------------------------------------

 "There's no reasonableness value for the timestamps: you
  can make a file look arbitrarily old or like it was
  modified at some time in the distant future (useful if
  you are writing sicence fiction stories)."

  -- "Learning Perl" by Randal L. Schwartz
      O'Reilly Press

----------------------------------------------------------

 "Mac for Productivity
  Palm for Portability
  Linux for Transparency
  BSD for Reliability
  Windows for... Solitaire."

 -- Mr X

----------------------------------------------------------

 "Flowers bloom and die
  Wind brings butterflies or snow
  A stone won't notice."

 -- Tyler Durden

----------------------------------------------------------

 "Being a smartass is better than being a dumbass."

 -- Terry Noble & Hideous Harpie
    Heard on Whole Wheat Radio

----------------------------------------------------------

 "I call for a separation of church and idiot."

 -- Mr X

----------------------------------------------------------

 "No dreams
  No disappointments."

 -- Tom Paris, Star Trek: Voyager

----------------------------------------------------------

 "Strange how someone can love someone else, and yet at 
  the same time pray for her to fall under a train."

 "This gem of, to me, incomprehensibility not only told
  me nothing, it convinced me once again that a mind
  as non-technical as mine should concern itself less
  with causes than effects."

 -- From Bob Shaw's short story: "Light of Other Days"

----------------------------------------------------------

 "Awwwwwwwwwwwwwwwwwww."
 "Awwww?"
 "Awwwwwwwww."

 -- Laura Douglass

----------------------------------------------------------

 "It is fast approaching the point where I don't want to
  elect anyone stupid enough to want the job."

 -- Erma Bombeck

----------------------------------------------------------

 "Democracy is a form of government that substitutes
  election by the incompetent many for appointment by a
  corrupt few"

 -- Mr X

----------------------------------------------------------

 "People the world would be better off without:
  - George W Bush
  - John Howard
  - Everyone in the "Family First Party"
  - Any member of the Taliban
  - Anyone from SCO"

 -- Ruben Schade

----------------------------------------------------------

 "I'm contemplating thinking about thinking..."
 
 -- Robbie Williams in his song "Come Undone"
    Escapology Albumn, 2003

----------------------------------------------------------



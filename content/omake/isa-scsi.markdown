---
title: "Adaptec ISA Cards"
---
This is based on my research to install a 50-pin ISA SCSI card in my vintage tower for my internal Iomega Zip and Jaz drives. For some reason, PCI-based cards work but impose a serious system-wide performance penalty.

### Key

* The DOS column refers to drivers available in Adaptec's official DOSDRVR.EXE bundle for DOS and Windows 3.x.

* Windows 95 column refers to Microsoft's built-in driver support. Presumably later drivers from Adaptec could also be sourced.

* BeOS, Red Hat 6.2 are pending, but would hopefully be available for the cards that have drivers below given they were from the same time period.

Model                   | DOS | Win95
------------------------|-----|-------
AVA-1502P, 1502AP       | ✅  | ❌
AVA-1505                | ✅  | ✅
AVA-1515                | ✅  | ✅
AVA-1510, 1520, 1522    | ✅  | ✅
AHA-1510A, 1520A, 1522A | ✅  | ❌ 
AHA-1510B, 1520B, 1522B | ✅  | 


---
layout: omake
title: "GPUs"
---

### Riva TNT cards

* Creative Graphics Blaster CT6700 
* DIAMOND Viper V550 (23230081-101)


### Cirrus Logic

Card                           | Chip
-------------------------------|--------
Creative GraphicsBlaster MA202 | CT6330

---
layout: omake
title: "SD-RAM"
---
I was trying to source double-sided RAM for my vintage Pentium 1 tower. Turned out to be harder than I thought.

DIMM                        | Capacity | Sided
----------------------------|----------|--------
[Siemens HYS64V8300GU-8]    | 64MiB    | Single
Toshiba THMY644041          | 32MiB    | Double
[Hyundai HYM7V65801]        | 64MiB    | Single
[Micron MT9LSDT872AG-IOEC7] | 64MiB    | Single

[Siemens HYS64V8300GU-8]: https://forum.pcmech.com/threads/need-help-with-identifying-ram-mb.147255/
[Hyundai HYM7V65801]: http://pcpartsoutlet.com/index.php/desktop-parts/memory-ram/hyundai-64mb-ddr-memory-168pin-pc100-hym7v65801-btfg-10s.html
[Micron MT9LSDT872AG-IOEC7]: https://picclick.com.au/MT9LSDT872AG-IOEC7-RAM-Micron-64MB-PC100-CL2-9C-8x8-361911545859.html


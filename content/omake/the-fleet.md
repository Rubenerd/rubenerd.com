---
layout: omake
title: "The fleet"
---
This page lists my current machines. I do have current Macs and a budget game machine, but most of my fun and joy comes from vintage computing.

### iYuki, the MacBook Air

Once I got my work MacBook Pro, iYuki was relegated to a Mac Mini with external peripherals. Aside from not being able to run a 4K display, she still does everything I need.

* **Apple 2012 11" MacBook Air**
    * Intel Core i5 1.7GHz
    * 8GiB DDR3-1600 memory
    * 64GiB TS064E Apple SSD

### Kyou and Mio, the companion cubes

These two Microservers serve as my FreeBSD ZFS NAS boxes. Kyou's specs have been updated to handle multiple Plex streams, Mio is fine with a Celeron and ECC memory for backups.

* **HP ProLiant Microserver Gen8**
    * Intel [Xeon E3-1260L] / [Celeron G1610T]
    * Intel [C204 chipset]
    * 16GiB ECC DDR3 memory
    * **Internal RAID**
        * Mix of 3/4/6TB Hitachi and WDs in ZFS mirrored pairs
    * Mellanox MT26428 ConnectX QDR IB

[Xeon E3-1260L]: https://ark.intel.com/products/52275/Intel-Xeon-Processor-E3-1260L-8M-Cache-2_40-GHz
[Celeron G1610T]: https://ark.intel.com/products/71074
[C204 chipset]: http://ark.intel.com/products/52804/Intel-BD82C204-PCH

### Tsuruya, the game tower

This was a small game machine I built once Apple ceded the professional graphics market. It's mostly used for Train Simulator, Cities Skylines, and other world builders.

It runs Windows 10, and Debian Jessie for when Windows fails. The parts were also carefully chosen in case I wanted to make a Hackintosh.

* NCASE M1 Version 5, Mini-ITX case
* Corsair SF450 SFX gold power supply
* **Gigabyte Z170N-WiFi Mini-ITX board**
    * Intel Core i5-6500 3.0GHz
    * 16GiB Crucial DDR4-2133 memory
    * **Onboard SATA3 bus**
        * 256GiB SanDisk Extreme SSD
        * 128GiB SanDisk Ultra SSD
    * ASUS Turbo GeForce GTX 970 OC Edition

### Ami, the Pentium 1 tower

This was the first computer I built as a kid growing up in Singapore in the late 1990s, with the help of the wonderful former Make Fine Computer in Funan Centre. It still runs today, save for a new PSU and memory.

My parents threw out our first 486 home computer, so I'm repurposing this machine as an all-in-one nostalgia box. The 5.25" drive, SoundBlaster card, MS-DOS 6.20 and Windows 3.0 were all from that original box.

It also runs Windows for Workgroups, Windows 95 (the first OS this machine ran), Windows NT 4.0 Workstation (that I always wanted it to run), and Red Hat Linux 6.3 (my first \*nix, installed from the original CDs).

* Mid 1990s ATX beige case
* Generic 300W AT power supply
* **Octek Rhino 12+ motherboard**
    * Intel Pentium MMX 200MHz CPU
    * **144MiB memory**
        * 128MiB PC-100 memory
        * 16MiB 72-pin SIMM
    * **Onboard IDE bus**
        * 4GiB SanDisk CF card
        * 8GiB SanDisk CF card
        * TEAC slimline CD-ROM
    * **Onboard FDD bus**
        * 1.44MiB 3.5" Mitsubishi disk drive
        * 1.2MiB 5.25" Panasonic disk drive
    * **PCI to IDE RAID controller card**
        * 32GiB SanDisk CF card
        * 32GiB SanDisk CF card
    * **Adaptec SCSI**
        * 100MiB SCSI Iomega Zip Drive
        * 1GB SCSI Iomega Jaz Drive
    * Matrox Mystique 200 VGA IDE card
    * 3Com 10BaseT PCI card
    * SoundBlaster AWE32 ISA sound card
    * ESS AudioDrive ES1868 ISA sound card

### PowerMac G3

My first Mac was a blueberry iMac in the late 1990s, but it long since died. This PowerMac came out at around the same time and has the same aesthetic, so I'm using it to rebuild a classic MacOS environment. 

* Blue and White PowerMac G3 enclosure
* **Apple Revision A PowerMac G3 logic board**
    * PowerPC G3 300MHz
    * 256MiB PC-133 memory
    * **Onboard IDE bus**
        * Sony DVD-ROM
    * **Sonnet PCI to IDE controller card**
        * 32GiB SanDisk CF card
        * 100MiB Iomega Zip Drive

### Commodore Plus/4

I was born after this machine came out, but this has to be the most beautiful hardware Commodore ever released.

---
layout: omake
title: "NAS computer cases"
---
This page lists my research into the ultimate case for a FreeBSD ZFS NAS build. As with most things, nothing ticks all the boxes! All we can do is get closer.


### Preferred specs

* 8-12 hard drives, or it's not worth it
   * Hot-swappable caddies built in, or
   * Generous 5.25" bays for after market hot-swap caddies
* Simple design
   * As little wasted space as possible
   * No superfluous LEDs, boy racer grills in spider shapes, etc!


### With hot swap drives

Case                      | Size      | 3.5 | 5.25 | Extra
--------------------------|-----------|-----|------|--------
[Chenbro SR105]           | ATX       | 4   | 3    | 4x 2.5"
[Chenbro SR209]           | ATX       | 4   | 3    | 4x 2.5"
[Chenbro SR107]           | E-ATX     | 8   | 3    | 1x FDD
[Chenbro SR112]           | E-ATX     | 8   | 2    | 2x HDD
[CFI-A789]                | Mini-ITX  | 4   | 0    |
[CFI-A1028]               | ATX++     | 6   | 6    | HUGE!
[CFI-A1068]               | ATX       | 6   | 4    | 
[CFI-A5000]               | Mini-ITX  | 7   | 1    |
[CFI-A7X79]               | Mini-ITX  | 4   | 0    |
[Fractal Design Array R2] | Mini-ITX  | 6   | 0    | 
[Fractal Design Node 304] | Mini-ITX  | 6   | 0    | 
[Fractal Design Node 804] | Micro-ATX | 8   | 0    | 
[In-Win IW-MS04]          | Mini-ITX  | 4   | 0.5  | SAS
[In-Win IW-PLV]           | E-ATX     | 12  | 1    | Wow!
[Norco ITX-S8]            | Mini-ITX  | 8   | 0    | 
[Silverstone DS380]       | Mini-ITX  | 8   | 0    | 
[Silverstone CS380]       | ATX       | 8   | 2    | 
[Supermicro SC742]        | 4U        | 7   | 3    | 1x FDD
[Supermicro SC743]        | 4U        | 7   | 3    | 1x FDD
[Supermicro SC747]        | 4U        | 8   | 3    | 1x FDD
[Thermaltake Level 10]    | ATX       | 6   | 3    | Wow!
[U-NAS NSC-800]           | Mini-ITX  | 8   | 0    | 
[U-NAS NSC-810]           | Mini-ITX  | 8   | 0    | 2x PCI

[Chenbro SR105]: http://www.chenbro.com/en-global/products/TowerServerChassis/Mid_range_chassis_for_SOHO_SMB/SR105
[Chenbro SR209]: http://www.chenbro.com/en-global/products/TowerServerChassis/Entry_level_chassis_for_SOHO/SR209
[Chenbro SR107]: http://www.chenbro.com/en-global/products/TowerServerChassis/High_End_chassis_for_Enterprise/SR107
[Chenbro SR112]: http://www.chenbro.com/en-global/products/TowerServerChassis/High_End_chassis_for_Enterprise/SR112
[CFI-A789]: https://www.logicsupply.com/a7879/
[CFI-A1028]: http://www.chyangfun.com/cfi-a1028.html
[CFI-A1068]: http://www.chyangfun.com/cfi-a1068.html
[CFI-A5000]: http://www.chyangfun.com/cfi-a5000.html
[CFI-A7X79]: http://www.chyangfun.com/cfi-a7x79.html
[Fractal Design Array R2]: http://www.fractal-design.com/home/product/cases/discontinued-products/array-r2-mini-itx-nas-case
[Fractal Design Node 304]: http://www.fractal-design.com/home/product/cases/node-series/node-304-black
[Fractal Design Node 804]: http://www.fractal-design.com/home/product/cases/node-series/node-804
[In-Win IW-MS04]: https://www.in-win.com/en/ipc-server/ms04
[In-Win IW-PLV]: https://www.in-win.com/en/ipc-server/plv%20tower
[Norco ITX-S8]: http://www.norcotek.com/product/itx-s8/
[Silverstone DS380]: http://www.silverstonetek.com/product.php?pid=452
[Silverstone CS380]: http://www.silverstonetek.com/product.php?pid=709
[Supermicro SC742]: http://www.supermicro.com/products/chassis/4U/742/SC742T-650.cfm
[Supermicro SC743]: http://www.supermicro.com/products/chassis/4U/743/SC743TQ-865B-S
[Supermicro SC747]: http://www.supermicro.com/products/chassis/4U/747/SC747BTQ-R1K62B
[Thermaltake Level 10]: http://www.thermaltake.com/products-model.aspx?id=C_00001903
[U-NAS NSC-800]: http://www.u-nas.com/xcart/product.php?productid=17617
[U-NAS NSC-810]: http://www.u-nas.com/xcart/product.php?productid=17640


### With just 5.25" drive bays

Case                            | Size      | 5.25" | Notes
--------------------------------|-----------|-------|--------
[AeroCool ZeroDegree-BK]        | ATX       | 9     |
[AZZA Helios 910]               | ATX       | 9     |
[Antec Nine Hundred]            | ATX       | 9     | V3
[ENERMAX ECA5010M-B-B]          | ATX       | 11    |
[Lian-Li PC-A79]                | ATX       | 12    | 
[Lian-Li PC-D600]               | ATX       | 10    | Thermal zone
[Lian-Li P80N]                  | ATX       | 12    | Pivot door
[Lian-Li PC-Q35B]               | Mini-ITX  | 5     | 
[CoolerMaster SGC-5000-KWN1]    | ATX       | 9     | Oh dear
[CoolerMaster CM Storm Trooper] | ATX       | 9     | Oh dear
[iStarUSA S-917]                | Mini-ITX  | 7     | Newegg
[iStarUSA S-919]                | Micro-ITX | 9     | Nobody sells?
[iStarUSA S-9112-EATX]          | EATX      | 12    | "Coming soon"
[Nexustek Edge]                 | ATX       | 13    | Discontinued :(
[SilverStone TJ11B-W]           | ATX       | 9     | 
[Thermaltake Core W100]         | EATX      | 11    |
[Thermaltake Core W200]         | EATX      | 10    |

[AeroCool ZeroDegree-BK]: https://www.newegg.com/Product/Product.aspx?Item=N82E16811196023
[AZZA Helios 910]: https://www.newegg.com/Product/Product.aspx?Item=N82E16811517007
[ENERMAX SPINEREX ECA5010M-B-B]: https://www.newegg.com/Product/Product.aspx?Item=N82E16811124141
[Lian-Li PC-Q35B]: http://www.lian-li.com/en/dt_portfolio/pc-q35/
[Lian-Li PC80N]: http://www.lian-li.com/en/dt_portfolio/pc-p80n/
[CoolerMaster CM Storm Trooper]: http://www.coolermaster.com/case/full-tower/trooper/
[CoolerMaster SGC-5000-KWN1]: http://www.coolermaster.com/case/full-tower/trooperwindow/
[Nine Hundred Two]: http://www.antec.com/product.php?id=705322&pid=43&lan=nz
[iStarUSA S-917]: http://www.istarusa.com/istarusa/products.php?model=S-917
[iStarUSA S-919]: http://www.istarusa.com/istarusa/products.php?model=S-919
[iStarUSA S-9112-EATX]: http://www.istarusa.com/istarusa/products.php?model=S-9112-EATX
[Nexustek Edge]: https://nexustek.us/cases/edge
[SilverStone TJ11B-W]: http://www.silverstonetek.com/product.php?pid=292
[Thermaltake Core W100]: http://www.thermaltake.com/Chassis/Super_Tower_/Core/C_00002802/Core_W100/Design.htm#


### With several 5.25" drive bays and internal 3.5

Case                          | Size | 3.5 | 5.25 
------------------------------|------|-----|------
[CFI-A8008]                   | ATX  | 6   | 4
[CoolerMaster CM 690 II Plus] | ATX  | 6   | 4
[CoolerMaster CMP-350]        | ATX  | 7   | 4
[CoolerMaster Silencio 652s]  | ATX  | 8   | 3
[Lian-Li PC-A75WX]            | HPTX | 12  | 2
[Lian-Li PC-A76]              | HPTX | 12  | 2
[Lian-Li PC-7FN]              | ATX  | 4   | 4

[CFI-A8008]: http://www.chyangfun.com/cfi-a8008.html
[CoolerMaster CM 690 II Plus]: http://www.coolermaster.com/case/mid-tower/cm-690-ii-plus-white/
[CoolerMaster CMP-350]: http://www.coolermaster.com/case/mid-tower/cmp-350/
[CoolerMaster Silencio 652s]: http://www.coolermaster.com/case/mid-tower/silencio652s/
[Lian-Li PC-A75WX]: http://www.ebay.com.au/itm/Lian-Li-PC-A75WX-Full-tower-HPTX-no-power-supply-ATX-black-PC-A75WX-/272538017305
[Lian-Li PC-A76]: http://www.lian-li.com/en/dt_portfolio/pc-a76/
[Lian-Li PC-7FN]: http://www.lian-li.com/en/dt_portfolio/pc-7fn/

These could be fitted with ICY DOCK or SuperMicro hot-swap trays.


### Consumer JBODs

Case          | eSATA3 | USB3 | 3.5
--------------|--------|------|-----
[CFI-B7843JD] | ✅     | ✅   | 4
[CFI-BXX46XM] | ✅     |      | 4
[CFI-BXX86CM] | ✅     |      | 8
[CFI-B7843JU] |        | ✅   | 4
[CFI-B7883JD] | ✅     | ✅   | 8
[CFI-B7883JU] |        | ✅   | 8

[CFI-B7843JD]: http://www.chyangfun.com/cfi-b7843jd-4-bay.html
[CFI-BXX46XM]: http://www.chyangfun.com/sata-6g-cfi-bxx46xm-4-bay.html
[CFI-BXX86CM]: http://www.chyangfun.com/sata-6g-cfi-bxx86cm-8-bay.html
[CFI-B7843JU]: http://www.chyangfun.com/cfi-b7843ju-4-bay.html
[CFI-B7883JD]: http://www.chyangfun.com/cfi-b7883jd-8-bay.html
[CFI-B7883JU]: http://www.chyangfun.com/cfi-b7883ju-8-bay.html


### Backplane Cage

Cage                    | Bays | HDDs |  Notes
------------------------|------|------|---------------
[Chenbro SK32303]       | 2    | 3    | 
[ICY DOCK MB973SP-B]    | 2    | 3    | Trayless
[ICY DOCK MB973SP-1B]   | 2    | 3    | + 3x USB 3
[ICY DOCK MB973SP-2B]   | 2    | 3    | + eSATA, USB 2
[SilverStone FS303]     | 2    | 3    | Trayless
[NORCO SS-300]          | 2    | 3    |
[NORCO SS-400]          | 3    | 4    |
[SilverStone FS304]     | 3    | 4    | Trayless
[Thermaltake Max 3504]  | 3    | 4    | Large vent
[Chenbro SK33502]       | 3    | 5    |
[ICY DOCK MB975SP-B]    | 3    | 5    | Ind. HDD power
[ICY DOCK MB975SP-B R1] | 3    | 5    |          
[NORCO SS-500]          | 3    | 5    |            
[SilverStone FS305]     | 3    | 5    | Trayless
[Supermicro CSE-M35TQ]  | 3    | 5    | 

[Chenbro SK33502]: http://www.chenbro.com/en-global/products/Storage_Expansion_Kit/Hot-Swap_HDD_Enclosure/SK33502
[ICY DOCK MB973SP-B]: http://www.icydock.com/goods.php?id=119
[ICY DOCK MB973SP-1B]: http://www.icydock.com/goods.php?id=157
[ICY DOCK MB973SP-2B]: http://www.icydock.com/goods.php?id=158
[ICY DOCK MB975SP-B]: http://www.icydock.com/goods.php?id=163
[ICY DOCK MB975SP-B R1]: http://www.icydock.com/goods.php?id=242
[NORCO SS-300]: http://www.norcotek.com/product/ss-300/
[NORCO SS-400]: http://www.norcotek.com/product/ss-400/
[NORCO SS-500]: http://www.norcotek.com/product/ss-500/
[Chenbro SK32303]: http://www.chenbro.com/en-global/products/Storage_Expansion_Kit/Hot-Swap_HDD_Enclosure/SK32303
[SilverStone FS303]: http://www.silverstonetek.com/product.php?pid=603&area=en
[SilverStone FS304]: http://www.silverstonetek.com/product.php?pid=604&area=en
[SilverStone FS305]: http://www.silverstonetek.com/product.php?pid=605&area=en
[Supermicro CSE-M35TQ]: http://www.supermicro.com/products/accessories/mobilerack/CSE-M35TQ.cfm
[Thermaltake Max 3503]: http://www.thermaltake.com/Storage/Rack_/_/C_00002934/Max_3503_SATA_HDD_Rack/design.htm
[Thermaltake Max 3504]: http://www.thermaltake.com/Storage/Rack_/_/C_00002935/Max_3504_SATA_HDD_Rack/design.htm


## Other useful parts

Part         | Description
-------------|-------------------------------------------
[ATXBRACKET] | Power and reset switches on a PCI bracket

[ATXBRACKET]: https://www.highspeedpc.com/ATX_Power_Bracket_p/atxbracket.htm

---
layout: omake
title: "Recommended"
---
This is a work in progress.

### Manga
<p></p>

<ul class="recommended manga">

<li><a title="Barakamon" href="https://www.comixology.com/Barakamon/comics-series/26511"><img src="https://rubenerd.com/files/2017/cover-barakamon@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-barakamon@2x.jpg 2x" alt="Barakamon" /></a></li>

<li><a title="Fairy Tail" href="https://www.comixology.com/Fairy-Tail/comics-series/56605"><img src="https://rubenerd.com/files/2017/cover-fairytail@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-fairytail@2x.jpg 2x" alt="Fairy Tail: I could never get into One Piece or the other long-running Jump franchises, but the fantastically implausible world of Fairy Tail is too much fun!" /></a></li>

<li><a title="Evangelion" href="https://www.comixology.com/Neon-Genesis-Evangelion/comics-series/1836"><img src="https://rubenerd.com/files/2017/cover-eva@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-eva@2x.jpg 2x" alt="Evangelion" /></a></li>

<li><a title="Ghost in the Shell" href="https://www.comixology.com/The-Ghost-in-the-Shell/comics-series/86487"><img src="https://rubenerd.com/files/2017/cover-ghostintheshell@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-ghostintheshell@2x.jpg 2x" alt="Ghost in the Shell" /></a></li>

<li><a title="The Melancholy of Haruhi Suzimiya" href="https://www.comixology.com/The-Melancholy-of-Haruhi-Suzumiya/comics-series/3003"><img src="https://rubenerd.com/files/2017/cover-haruhi@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-haruhi@2x.jpg 2x" alt="The Melancholy of Haruhi Suzumiya" /></a></li>

<li><a title="Steins;Gate" href="http://www.udonentertainment.com/blog/news/steinsgate2"><img src="https://rubenerd.com/files/2017/cover-steinsgate@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-steinsgate@2x.jpg 2x" alt="Steins;Gate" /></a></li>

</ul>

### Podcasts
<p></p>

<ul class="recommended podcasts">

<li><a href="https://stilgherrian.com/the_9pm_edict/" title="The 9PM Edict: He's opinionated, informed, hilarious, witty, modest, intelligent, and often right"><img src="https://rubenerd.com/files/2017/cover-9pm-edict@1x.png" srcset="https://rubenerd.com/files/2017/cover-9pm-edict@2x.png 2x" alt="The 9PM Edict: He's opinionated, informed, hilarious, witty, modest, intelligent, and often right." /></a></li>

<li><a href="http://5by5.tv/b2w/" title="Back to Work: Dan Benjamin and Merlin Mann are delightful, and always have good ideas. Bauk Bauk!"><img src="https://rubenerd.com/files/2017/cover-b2w@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-b2w@2x.jpg 2x" alt="Back to Work: Dan Benjamin and Merlin Mann are delightful, and always have good ideas. Bauk Bauk!" /></a></li>

<li><a href="https://techcrunch.com/video/gillmor-gang/"><img src="https://rubenerd.com/files/2017/cover-gillmor-gang@1x.png" srcset="https://rubenerd.com/files/2017/cover-gillmor-gang@2x.png 2x" alt="The Gillmor Gang: One of the first podcasts I ever listened to going back to 2004, and they've still got it!" /></a></li>

<li><a href="http://www.abc.net.au/radionational/programs/lawreport/"><img src="https://rubenerd.com/files/2017/cover-radionational@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-radionational@2x.jpg 2x" alt="The Law Report: a fascinating, plain English discussion of legal topics and issues in Australia." /></a></li>

<li><a href="http://www.theminimalists.com/podcast/"><img src="https://rubenerd.com/files/2017/cover-minimalists@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-minimalists@2x.jpg 2x" alt="The Minimalists: How to live a meaninful life with less. You could say I get value from it." /></a></li>
   
<li><a href="http://www.noagendashow.com/"><img src="https://rubenerd.com/files/2017/cover-na@1x.png" srcset="https://rubenerd.com/files/2017/cover-na@2x.png 2x" alt="No Agenda: Comedy, conjecture, crap, and correctness in equal doses, but ALWAYS entertaining. In the morning, slave!" /></a></li>

<li><a href="http://www.merlinmann.com/roderick/"><img src="https://rubenerd.com/files/2017/cover-roderick@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-roderick@2x.jpg 2x" alt="Roderick on the Line. Merlin Mann, with John Roderick of The Long Winters. It's the chocolate box of podcasts" /></a></li>

<li><a href="http://onsug.com/"><img src="https://rubenerd.com/files/2017/cover-onsug@1x.png" srcset="https://rubenerd.com/files/2017/cover-onsug@2x.png 2x" alt="The Overnightscape Underground, featuring most of my favourite shows ever." /></a></li>

<li><a href="https://rubenerd.com/show/"><img src="https://rubenerd.com/files/2017/cover-rubenerdshow@1x.png" srcset="https://rubenerd.com/files/2017/cover-rubenerdshow@2x.png 2x" alt="The Rubénerd Show: Because a little shameless self-promotion almost never hurt anyone." /></a></li>

<li><a href="https://www.samharris.org/podcast/"><img src="https://rubenerd.com/files/2017/cover-waking-up@1x.png" srcset="https://rubenerd.com/files/2017/cover-waking-up@2x.png" alt="Waking Up with Sam Harris: Philosophy, science, meditation and reason with an endlessly fascinating roster of guests." /></a></li>

<li><a href="http://www.wellmaywesay.com/"><img src="https://rubenerd.com/files/2017/cover-wellmaywesay@1x.jpg" srcset="https://rubenerd.com/files/2017/cover-wellmaywesay@2x.jpg 2x" alt="Well May We Say: Compassion and reason in a world increasingly devoid of both." /></a></li>

</ul>


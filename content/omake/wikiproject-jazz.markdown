---
layout: omake
title: "WikiProject Jazz" 
---
A long term project is overhauling and updating Wikipedia's neglected jazz catalogue. I'm tracking changes here.


### Links

* [WikiProject Albums](https://en.wikipedia.org/wiki/Wikipedia:WikiProject Albums)
* [WikiProject Jazz](https://en.wikipedia.org/wiki/Wikipedia:WikiProject Jazz)


### Created Michael Franks articles

* [Abandoned Garden](https://en.wikipedia.org/wiki/Abandoned_Garden)
* [Barefoot on the Beach](https://en.wikipedia.org/wiki/Barefoot_on_the_Beach)
* [Blue Pacific (album)](https://en.wikipedia.org/wiki/Blue_Pacific_(album))
* [Burchfield Nines](https://en.wikipedia.org/wiki/Burchfield_Nines)
* [Dragonfly Summer](https://en.wikipedia.org/wiki/Dragonfly_Summer)
* [Indispensable (Michael Franks album)](https://en.wikipedia.org/wiki/Indispensable_(Michael_Franks_album))
* [Love Songs (Michael Franks album)](https://en.wikipedia.org/wiki/Love_Songs_(Michael_Franks_album))
* [Michael Franks (album)](https://en.wikipedia.org/wiki/Michael_Franks_(album))
* [Michael Franks with Crossfire Live](https://en.wikipedia.org/wiki/Michael_Franks_with_Crossfire_Live)
* [Objects of Desire](https://en.wikipedia.org/wiki/Objects_of_Desire)
* [One Bad Habit](https://en.wikipedia.org/wiki/One_Bad_Habit)
* [Passionfruit (album)](https://en.wikipedia.org/wiki/Passionfruit_(album))
* [Rendezvous in Rio](https://en.wikipedia.org/wiki/Rendezvous_in_Rio)
* [Skin Dive](https://en.wikipedia.org/wiki/Skin_Dive)
* [Sleeping Gypsy (album)](https://en.wikipedia.org/wiki/Sleeping_Gypsy_(album))
* [The Art of Tea](https://en.wikipedia.org/wiki/The_Art_of_Tea)
* [The Best of Michael Franks: A Backward Glance](https://en.wikipedia.org/wiki/The_Best_of_Michael_Franks:_A_Backward_Glance)
* [The Camera Never Lies](https://en.wikipedia.org/wiki/The_Camera_Never_Lies)
* [The Dream 1973-2011](https://en.wikipedia.org/wiki/The_Dream_1973-2011)
* [The Michael Franks Anthology: The Art of Love](https://en.wikipedia.org/wiki/The_Michael_Franks_Anthology:_The_Art_of_Love)
* [Time Together](https://en.wikipedia.org/wiki/Time_Together)
* [Watching the Snow](https://en.wikipedia.org/wiki/Watching_the_Snow)


### Created Ben Sidran albums

* [Cool Paradise](https://en.wikipedia.org/wiki/Cool_Paradise)
* [Dylan Different](https://en.wikipedia.org/wiki/Dylan_Different)
* [Nick's Bump](https://en.wikipedia.org/wiki/Nick's_Bump)


### Created Pizzarelli albums

* [Green Guitar Blues](https://en.wikipedia.org/wiki/Green_Guitar_Blues)
* [Summer Night: Live](https://en.wikipedia.org/wiki/Summer_Night:_Live)
* [I'm Hip (Please Don't Tell My Father)](https://en.wikipedia.org/wiki/I'm_Hip_(Please_Don't_Tell_My_Father))

### Created Steve Tyrell albums

* [Back to Bacharach](https://en.wikipedia.org/wiki/Back_to_Bacharach)


---
layout: omake
title: "Lian Li Drives"
---
This page lists Lian Li's cases and their hard drive capacities. It's maddening that we're in 2017 barely anyone makes a NAS-optimised pedestal enclosure for 8+ drives that doesn't have a ton of wasted space for graphics cards etc we don't need.

Cases with "N or M" can have their 5.25" bays swapped for drive caddies by default. Others can use after market trays from ICY DOCK or SuperMicro to convert extra 5.25" bays for 3.5" drives.

Case       | Size     | 3.5  | 5.25  | Notes
-----------|----------|------|-------|----------
PC-Q08     | Mini-ITX | 6    | 1     |
PC-Q25     | Mini-ITX | 7    | -     |
PC-Q35     | Mini-ITX | -    | 5     |
PC-7N      | ATX      | 7    | 2     | Only 4 HDD trays
PC-K6(S)   | ATX      | 7    | 2     | S has ugly door
PC-8E      | ATX      | 5    | 4     |
PC-A61     | ATX      | 6    | 4     | 
PC-B16     | ATX      | 6    | 4     | 
PC-10N     | ATX      | 6    | 3     |
PC-Z60     | ATX      | 6    | 3     |
PC-9F      | ATX      | 6    | 3     |
PC-08S     | E-ATX    | 6    | -     | Big
PC-V1000L  | E-ATX    | 10   | 1     | Old Mac Pro
PC-D600    | E-ATX    | 6 or 0  | 3 or 12  | Box!
PC-A79     | E-ATX    | 9 or 0  | 3 or 18 | Can swap
PC-X2000FN | E-ATX    | 9    | 2     | Can swap

PC-Q26 | 11!

[PC-Q25]: http://www.lian-li.com/en/dt_portfolio/pc-q25/
[PC-K6S]: http://www.lian-li.com/en/dt_portfolio/pc-k6s/

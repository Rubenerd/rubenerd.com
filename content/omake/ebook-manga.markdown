---
layout: omake
title: "eBook manga"
---
I'm trying to get rid of stuff, and buying replacement electronic versions wherever I can. Ideally one store would have everything.

### Key
* Bw = BookWalker
* cX = Comixology
* Ki = Kindle (Amazon)

### Search boxes

<p></p>

<form action="https://global.bookwalker.jp/search/" method="get">
<fieldset>
<input id="searchtextbox" type="text" name="word" value="" class="f-keyword no-lang-select" />
<input type="submit" value="Search BookWalker" class="f-submit" />
</fieldset>
</form>

<form action="https://www.comixology.com/search" method="get">
<fieldset>
<input type="text" name="search" value="" />
<input type="submit" value="Search comiXology" />
</fieldset>
</form>

<form action="https://www.comixology.com/search/items" method="get">
<fieldset>
<input type="text" name="search" value="" />
<input type="hidden" name="subType" value="COLLECTIONS" />
<input type="submit" value="Search comiXology (Graphic Novels &amp; Collections)" />
</fieldset>
</form>

<form action="https://www.comixology.com/search/series" method="get">
<fieldset>
<input type="text" name="search" value="" />
<input type="submit" value="Search comiXology (Series)" />
</fieldset>
</form>

<form accept-charset="utf-8" action="https://www.amazon.com/s/ref=nb_sb_noss_2" class="nav-searchbar" method="GET" name="site-search" role="search">
<fieldset>
<input type="hidden" value="search-alias=digital-text" />
<input type="text" id="twotabsearchtextbox" value="Yotsuba" name="field-keywords" autocomplete="off" placeholder="Search in Kindle Store" class="nav-input" tabindex="19" data-com.agilebits.onepassword.user-edited="yes">
<input type="submit" class="nav-input" value="Search Kindle" tabindex="20">
</fieldset>
</form>

<form action="https://www.kobo.com/en/search/" method="get">
<fieldset>
<input type="text" name="Query" value="" />
<input type="hidden" name="fcmedia" value="Book" />
<input type="hidden" name="id" value="84a1c203-0341-41aa-898d-a745eb6e6b36" />
<input type="submit" value="Search Kobo (eBooks &gt; Comics &amp; Graphic Novels)" />
</fieldset>
</form>

<p></p>

### Who has what

Title                                                       | Bw | cX | Ki |
------------------------------------------------------------|----|----|----|
A Certain Magical Index                                     | ✅ | ❌ | ❌ |
<span style="color:red">A Certain Scientific Railgun</span> | ❌ | ❌ | ❌ |
A Silent Voice                                              | ✅ | ✅ | ✅ |
Akame Ga Kill!                                              | ✅ | ✅ | ❌ |
↳  Zero                                                     | ✅ | ✅ | ❌ |
Aldnoah.Zero                                                | ✅ | ❌ | ❌ |
Baccano!                                                    | ✅ | ❌ | ❌ |
Bamboo Blade                                                | ✅ | ✅ | ❌ |
Barakamon                                                   | ✅ | ✅ | ❌ |
Bakuman                                                     | ❌ | ✅ | ✅ |
Black Butler                                                | ✅ | ✅ | ❌ |
Blue Exorcist                                               | ❌ | ✅ | ✅ |
Bungo Stray Dogs                                            | ✅ | ❌ | ❌ |
Cage of Eden                                                | ✅ | ✅ | ✅ |
Cardcaptor Sakura                                           | ❌ | ✅ | ✅ |
Cells at Work                                               | ✅ | ✅ | ✅ |
<span style="color:red">Code Geass</span>                   | ❌ | ❌ | ❌ |
Coppelion                                                   | ✅ | ✅ | ✅ |
D-Frag                                                      | ✅ | ✅ | ✅ |
DanMachi                                                    | ✅ | ✅ | ❌ |
The Devil is a Part-Timer                                   | ✅ | ❌ | ❌ |
Evangelion                                                  | ✅ | ✅ | ✅ |
↳  Raising Project                                          | ❌ | ✅ | ✅ |
Fairy Tail                                                  | ✅ | ✅ | ✅ |
↳  Blue Mistral                                             | ✅ | ✅ | ✅ |
↳  Fairy Girls                                              | ✅ | ✅ | ✅ |
Fate/Stay Night                                             | ✅ | ✅ | ✅ |
Fate/Zero                                                   | ❌ | ✅ | ✅ |
Flying Witch                                                | ❌ | ✅ | ✅ |
From the New World                                          | ✅ | ✅ | ✅ |
Food Wars                                                   | ✅ | ✅ | ✅ |
<span style="color:red">Fruits Basket</span>                | ❌ | ❌ | ✅ |
Ghost in the Shell                                          | ✅ | ✅ | ✅ |
↳  Stand Alone Complex                                      | ✅ | ✅ | ✅ |
Handa-kun                                                   | ✅ | ✅ | ❌ |
<span style="color:red">Hayate the Combat Butler</span>     | ❌ | ❌ | ❌ |
Highschool of the Dead                                      | ✅ | ❌ | ❌ |
Horimiya                                                    | ✅ | ✅ | ❌ |
How to Raise a Boring Girlfriend                            | ✅ | ✅ | ❌ |
I'm Sakamoto                                                | ✅ | ❌ | ❌ |
In/Spectre                                                  | ✅ | ✅ | ✅ |
<span style="color:red">Inu x Boku SS</span>                | ❌ | ❌ | ❌ |
The Isolator                                                | ✅ | ✅ | ❌ |
Jojo's Bizzare Adventure                                    | ❌ | ✅ | ✅ |
K-On!                                                       | ✅ | ❌ | ❌ |
↳  College                                                  | ✅ | ❌ | ❌ |
↳  High School                                              | ✅ | ❌ | ❌ |
Kagerou Daze                                                | ✅ | ❌ | ❌ |
Konosuba                                                    | ✅ | ✅ | ❌ |
Love Hina                                                   | ✅ | ✅ | ✅ |
<span style="color:red">Love Live</span>                    | ❌ | ❌ | ❌ |
Lucky★Star                                                  | ✅ | ✅ | ✅ |
Mayo Chiki!                                                 | ✅ | ✅ | ✅ |
Magika Swordsman and Summoner                               | ✅ | ✅ | ✅ |
Melancholy of Suzumiya Haruhi                               | ✅ | ✅ | ❌ |
↳  Chan                                                     | ✅ | ✅ | ❌ |
Merman in My Tub                                            | ✅ | ✅ | ✅ |
Misfortune of Kyon and Koizumi                              | ✅ | ✅ | ❌ |
Monthly Girls' Nozaki-kun                                   | ✅ | ✅ | ❌ |
Mysterious Girlfriend X                                     | ✅ | ✅ | ✅ |
Negima                                                      | ✅ | ✅ | ✅ |
Nisekoi                                                     | ❌ | ✅ | ✅ |
Non Non Biyori                                              | ✅ | ✅ | ✅ |
Occultic;Nine                                               | ✅ | ❌ | ✅ |
One Punch Man                                               | ❌ | ✅ | ✅ |
Ouran High Hosts Club                                       | ❌ | ❌ | ✅ |
<span style="color:red">Persona 3</span>                    | ❌ | ❌ | ❌ |
<span style="color:red">Persona 4</span>                    | ❌ | ❌ | ❌ |
Persona Q                                                   | ✅ | ✅ | ✅ |
Rurouni Kenshin                                             | ❌ | ✅ | ✅ |
↳  Restoration                                              | ❌ | ✅ | ✅ |
<span style="color:red">Sailor Moon</span>                  | ❌ | ❌ | ❌ |
Saki                                                        | ✅ | ✅ | ❌ |
Sankarea                                                    | ✅ |    |    |
Seraph of the End                                           | ❌ | ✅ | ✅ |
Servant x Service                                           | ✅ | ✅ | ❌ |
<span style="color:red">Shakugan no Shana</span>            | ❌ | ❌ | ❌ |
Sherlock Bones                                              | ✅ | ✅ | ✅ |
Soul Eater                                                  | ✅ | ✅ | ❌ |
↳  NOT!                                                     | ✅ | ✅ | ❌ |
<span style="color:red">Steins;Gate</span>                  | ❌ | ❌ | ❌ |
Space Brothers                                              | ✅ | ✅ | ✅ |
Strike the Blood                                            | ✅ | ✅ | ❌ |
Sweetness and Lightning                                     | ✅ | ✅ | ✅ |
Testament of Sister New Devil                               | ❌ | ✅ | ✅ |
Triage X                                                    | ✅ | ✅ | ✅ |
Trinity Blood                                               | ✅ | ✅ | ✅ |
Toppu GP                                                    | ❌ | ✅ | ✅ |
Tokyo Ghoul                                                 | ❌ | ✅ | ✅ |
Trinity Seven                                               | ✅ | ✅ | ❌ | 
Umineko                                                     | ✅ | ✅ | ❌ |
UQ Holder                                                   | ✅ | ✅ | ✅ |
Yona of the Dawn                                            | ❌ | ✅ | ✅ |
Yotsuba                                                     | ✅ | ❌ | ✅ |
Zero no Tsukaima                                            | ✅ | ❌ | ✅ |


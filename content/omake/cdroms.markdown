---
layout: omake
title: "CD-ROMs"
---
This page lists my (culled!) CD-ROM collection.

### Mac

Sleeve | Title                         | Year
-------|-------------------------------|------
1      | Mac OS 9                      |
2      | Mac OS X 10.0                 |
3      | Mac OS X 10.0 Developer Tools |
3      | Mac OS X 10.5 Leopard         |

### Linux

Sleeve | Title                           | Year
-------|---------------------------------|------
10     | Red Hat Linux 6.2 (1-2)         |
       | Red Hat Linux 6.2 (2-2)         |
       | Red Hat Linux 6.2 Documentation |

### Windows Games

Sleeve | Title                                        | Year
-------|----------------------------------------------|-------
       | Microsoft Creative Writer and Fine Artist    |
       | Microsoft Creative Writer 2.0                |
       | Microsoft Flight Simulator 95                |
       | Microsoft Flight Simulator 97                |
       | Microsoft Flight Simulator 2000, disc 1 of 2 |
       | Microsoft Flight Simulator 2000, disc 2 of 2 |
       | Microsoft Golf Multimedia Edition            |
       | Microsoft Golf 3.0                           |

### Windows Software

Sleeve | Title                  | Year
-------|------------------------|------
       | Microsoft Publisher 97 |
       | Microsoft Publisher 98 |

### Windows Reference

Sleeve | Title                                  | Year
-------|----------------------------------------|------
       | Discover Malaysia (1-2)                |
       | Discover Malaysia (2-2)                |
       | DK How Things Work                     |
       | DK World Reference Atlas               |
       | Microsoft Dangerous Creatures          |
       | Microsoft Dinosaurs                    |
       | Microsoft Explorapedia World of Nature |
       | Microsoft Explorapedia World of People |
       | Microsoft Oceans                       |
       | Softkey The Complete Chef              |
       | Softkey World of Wine                  | 


---
layout: omake
title: "Engine room"
---
<p><img src="/cog_font_awesome.svg" alt="" style="width:96px; height:96px; float:right; margin:0 0 20px 30px;" /></p>

### What runs the site

* **[Hugo](http://gohugo.io)**. After running dynamic CMSs for years (see below), I got on the static site generator bandwagon in 2013. Of all the ones I've tried, Hugo is the only one that can handle 5,000 posts.

* **[FreeBSD](https://www.freebsd.org/)**: Still my OS weapon of choice since I started hacking on it in high school. I run it in the [Joviam cloud](https://joviam.com.au) with ZFS, pf and the tirelessly maintained ports system.

* **[nginx](https://nginx.org/)**: Fast, simple to configure web server and reverse proxy. Thanks to the maintainers of the [nginx-devel](https://www.freshports.org/www/nginx-devel) FreeBSD port.

* **[Let's Encrypt](https://letsencrypt.org/)**: I bought HTTPS certs in the past, but Let's Encrypt makes the process so simple there's no point not using it.
It can also handle subdomains without having to pay extra (or at all).</p></li>

* **[Ansible](https://www.ansible.com/)**: All the site configuration, package installs and updates are carried out with Ansible playbooks.

* **[Bourne shell scripts](https://en.wikipedia.org/wiki/Bourne_shell)**: These are the glue for everything else, for podcast pages, encoding audio, scaling Retina&trade; images, uploading generated assets, and other tasks.

<hr />
### What used to run the site

<ul>
<li><p><strong><a href="http://jekyllrb.org">Jekyll</a> (2013-15)</strong>:
My return to static-site generation after nearly a decade. <em>Rubénerd</em> was being delivered
faster, used less resources, and had full revision history and tracking in a Git repo! Alas
it took over 20 minutes to generate all my posts.</p></li>

<li><p><strong><a href="http://wordpress.org/">WordPress</a> (2006-13)</strong>:
The Mac Daddy of blogging software. Movable Type had gone commercial at that stage, and
Radio UserLand looked to be in its last throes, so I followed the pack to WordPress. For all
the security issues and poorly written plugins, it served me surprisingly well for many
years.</p></li>

<li><p><strong><a href="http://realmacsoftware.com/rapidweaver/">RapidWeaver</a> (2005)</strong>:
An intruiging and pleasent Mac application that generated static pages, but I soon ran into
its limitations.</p></li>

<li><p><strong><a href="http://perl.org/">Perl CGI scripts</a> (2004):</strong>
I wrote my first site engine when at my first job out of high school, before university
started. It used CGI, which was a terrible idea but not enough people went to it to spawn
too many threads, so I avoided disaster!</p></li>
</ul>


<hr />
### How I write posts

<p><img src="/edit_font_awesome.svg" alt="" style="width:96px; height:96px; float:right; margin:0 0 20px 30px;" /></p>

<ul>
<li><p><strong><a href="http://vim.org">Vim</a></strong>:
I&rsquo;ve used nano, joe, emacs (albeit briefly), TextMate, Sublime Text and Atom, but I keep
coming back to this inexplicably wonderful mode of typing each time. I&rsquo;m still learning new
things with it, while likely forgetting other things.</p></li>

<li><p><strong><a href="http://brettterpstra.com/projects/nvalt/">nvALT</a></strong>:
A fantastic note-taking tool that splits ideas into separate, searchable text files. It&rsquo;s
akin to having your own text-based wiki.</p></li>

<li><p><strong><a href="http://www.apple.com/sg/macbook-air/">11&rdquo; MacBook Air</a></strong>:
I&rsquo;m still surprised how versitile this machine is. It fits on every coffee shop and
aeroplane table, but still packs enough punch for photo editing and dev work with a docking
station and peripherals. All I miss is Parallels working fast enough for games, and the
Retina screen from my work machine.</p></li>

<li><p><strong><a href="https://www.microsoft.com/accessories/en-us/products/keyboards/sculpt-ergonomic-desktop/l5v-00001">Microsoft Sculpt Ergonomic Keyboard</a></strong>:
I prefer mechanical keyboards, but went the split-keyboard route when I started getting
upper-arm pain. It&rsquo;s surprisingly snappy, especially compared to the mushy plastic feel of
the previous models.</p></li>

<li><p><strong><a href="http://www.michaelfranks.com/">Michael Franks</a></strong>:
My jam since I was a kid. He&rsquo;s a jazzy singler/songwriter with witty lyrics and albums for
every mood and time of day stretching over his half-century career. Incidently, I wrote all
but two of his albums on Wikipedia!</p></li>

<li><p><strong><a href="http://us.akg.com/akg-product-detail_us/k551slv.html">AKG K551</a></strong>:
I bought these headphones as per Marco Arment&rsquo;s recommendation. I&rsquo;m not all about the bass
(sorry Meghan Trainor) so these headphones sound amazing. The level of midrange detail
continues to blow my socks off, and the giant pads are soft and comfortable.</p></li>
</ul>

